package com.fearless.prototype.fearless_change_cards.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.library.GridViewActivity;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.Keyboard;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.RegisterPopup;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

import java.util.Objects;

public class ProfileActivity extends AppCompatActivity {

    RegisterPopup registerPopup;
    User user;
    Button registerButton;
    TextView userNameTextView;
    TextView userNameHeaderTextView;
    TextView notRegisteredTextView;
    UserViewModel userViewModel;
    BottomNavigationView selectedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        notRegisteredTextView = findViewById(R.id.not_registered_text);
        userNameHeaderTextView = findViewById(R.id.username_header);

        BottomNavigationItemView profile = findViewById(R.id.card_of_the_day);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToCardOfDay();
            }
        });
        selectedView = findViewById(R.id.bottomNavigationView);
        userNameTextView = findViewById(R.id.user_name_text);

        selectedView.setSelectedItemId(R.id.profile_btn);


        registerButton = findViewById(R.id.profile_register_user_btn);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        ProfileActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                registerPopup = new RegisterPopup(
                        Objects.requireNonNull(inflater),
                        userViewModel,
                        ProfileActivity.this,
                        new Observer<User>() {
                            @Override
                            public void onChanged(@Nullable User user) {
                                showUserInfo(user);
                            }
                        });

                registerPopup.showAtLocation(v, Gravity.CENTER, 0, 0);
                Keyboard.showKeyboard(ProfileActivity.this);
            }
        });


        userViewModel.queryRegisteredUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                ProfileActivity.this.user = user;
                showUserInfo(user);
            }
        });
    }


    private void showUserInfo(User user) {
        if (user != null && user.getSelfLink() != null && !user.getSelfLink().isEmpty()) {
            this.userNameTextView.setText(user.getUsername());
            this.userNameTextView.setVisibility(View.VISIBLE);
            this.userNameHeaderTextView.setVisibility(View.VISIBLE);
            this.registerButton.setVisibility(View.GONE);
            this.notRegisteredTextView.setVisibility(View.GONE);
        }
    }

    public void switchToCardOfDay() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);

    }

    public void switchToLibrary() {
        Intent myIntent = new Intent(this, GridViewActivity.class);
        startActivity(myIntent);
    }

}
