package com.fearless.prototype.fearless_change_cards.data.source.remote;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.ChallangeList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.PatternList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.StrategyList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.Challenge;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemotePattern;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteStrategy;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;
import com.fearless.prototype.fearless_change_cards.data.source.remote.mappers.ChallengeScenarioMapper;
import com.fearless.prototype.fearless_change_cards.data.source.remote.mappers.PatternMapper;
import com.fearless.prototype.fearless_change_cards.data.source.remote.mappers.StrategyMapper;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RemoteScenarioDataSource implements ScenarioDataSource {


    private final PatternRestClient patternRestClient;


    public RemoteScenarioDataSource() {
        Retrofit retrofit = Injection.provideRetrofit();
        patternRestClient = retrofit.create(PatternRestClient.class);
    }


    @Override
    public void getScenarios(@NonNull GetScenariosCallback callback) {
        GetScenariosTask task = new GetScenariosTask(callback, patternRestClient);
        task.execute();
    }

    private static class GetScenariosTask extends AsyncTask<Void, Void, List<Scenario>> {
        private final GetScenariosCallback callback;
        private final PatternRestClient restClient;

        GetScenariosTask(GetScenariosCallback callback, PatternRestClient restClient) {
            this.callback = callback;
            this.restClient = restClient;
        }

        @Override
        protected List<Scenario> doInBackground(Void... voids) {
            try {
                Response<ChallangeList> scenarioRepsone = restClient.getAllChallanges()
                        .execute();
                if (scenarioRepsone.isSuccessful()) {
                    List<Challenge> challenges = scenarioRepsone.body().getChallenges();
                    List<Scenario> scenarios = new ArrayList<>();
                    Long i = 0L;
                    for (Challenge remoteScenario : challenges) {
                        Scenario scenario = ChallengeScenarioMapper.map(remoteScenario);
                        scenario.setPrimaryId(i);
                        addStrategiesToScenario(scenario, remoteScenario.getStrategies().getHref(), restClient);
                        scenarios.add(scenario);
                        i++;
                    }
                    return scenarios;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(List<Scenario> scenarios) {
            if (scenarios == null)
                callback.onDataNotAvailable();
            else
                callback.onScenariosLoaded(scenarios);
        }
    }

    @Override
    public void getScenario(@NonNull String scenarioId, @NonNull GetScenarioCallback callback) {
        GetScenarioTask task = new GetScenarioTask(scenarioId, patternRestClient, callback);
        task.execute();
    }


    private static class GetScenarioTask extends AsyncTask<Void, Void, Scenario> {

        private final GetScenarioCallback callback;
        private final String scenarioId;
        private final PatternRestClient restclient;

        GetScenarioTask(String scenarioId, PatternRestClient client, GetScenarioCallback callback) {
            this.scenarioId = scenarioId;
            this.callback = callback;
            this.restclient = client;
        }

        @Override
        protected Scenario doInBackground(Void... voids) {
            try {
                Long id = Long.parseLong(scenarioId);
                Response<ChallangeList> challangeListResponse = restclient.getChallangeAtIndex(id).execute();

                if (challangeListResponse.isSuccessful()) {
                    List<Challenge> challengeList = challangeListResponse.body().getChallenges();
                    if (!challengeList.isEmpty()) {
                        Scenario scenario = ChallengeScenarioMapper.map(challengeList.get(0));
                        scenario.setPrimaryId(id);
                        RemoteScenarioDataSource.addUserInfoToScenario(scenario, restclient);
                        RemoteScenarioDataSource.addStrategiesToScenario(scenario,
                                challengeList.get(0).getStrategies().getHref(),
                                restclient);
                        return scenario;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null)
                callback.onScenarioLoaded(scenario);
            else
                callback.onDataNotAvailable();
        }
    }


    @Override
    public void saveScenario(@NonNull Scenario scenario, @NonNull GetScenarioCallback callback) {
        if (scenario.getSelfLink() == null || scenario.getSelfLink().isEmpty())
            return;
        SaveScenarioTask task = new SaveScenarioTask(scenario, patternRestClient, callback);
        task.execute();
    }

    private static class SaveScenarioTask extends AsyncTask<Void, Void, Scenario> {


        private final Scenario scenario;
        private final PatternRestClient restClient;
        private final GetScenarioCallback callback;

        SaveScenarioTask(Scenario scenario, PatternRestClient restClient, GetScenarioCallback callback) {
            this.scenario = scenario;
            this.restClient = restClient;
            this.callback = callback;
        }

        @Override
        protected Scenario doInBackground(Void... voids) {
            try {
                Response<Challenge> challengeResponse = restClient.
                        putChallange(scenario.getSelfLink(), ChallengeScenarioMapper.map(scenario)).execute();
                if (challengeResponse.isSuccessful()) {
                    return scenario;
                }
            } catch (
                    IOException e)

            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null)
                callback.onScenarioLoaded(scenario);
            else
                callback.onDataNotAvailable();
        }
    }


    @Override
    public void insertScenario(@NonNull Scenario scenario, @NonNull GetScenarioCallback callback) {
        InsertScenarioTask task = new InsertScenarioTask(scenario, patternRestClient, callback);
        task.execute();
    }

    private static class InsertScenarioTask extends AsyncTask<Void, Void, Scenario> {
        private final Scenario scenario;
        private final GetScenarioCallback callback;
        private final PatternRestClient restClient;

        public InsertScenarioTask(Scenario scenario, PatternRestClient restClient, GetScenarioCallback callback) {
            this.scenario = scenario;
            this.callback = callback;
            this.restClient = restClient;
        }


        @Override
        protected Scenario doInBackground(Void... voids) {
            try {
                Challenge challenge = ChallengeScenarioMapper.map(scenario);
                Response<Challenge> response = this.restClient.postChallenge(challenge).execute();
                if (response.isSuccessful()) {
                    challenge = response.body();
                    RequestBody body = RequestBody.create(MediaType.parse("text/plain"), scenario.getCreatorLink());
                    Response<Void> res = restClient.postLink(
                            challenge.getCreator().getHref(),
                            body).execute();
                    if (res.isSuccessful()) {
                        Scenario scenarios = ChallengeScenarioMapper.map(challenge);
                        return scenarios;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null)
                callback.onScenarioLoaded(scenario);
            else
                callback.onDataNotAvailable();
        }
    }


    @Override
    public void getScenarioCreator(@NonNull Scenario scenario, @NonNull ScenarioCreatorCallback callback) {
        GetScenarioCreatorTask task = new GetScenarioCreatorTask(patternRestClient, scenario, callback);
        task.execute();
    }

    private static class GetScenarioCreatorTask extends AsyncTask<Void, Void, User> {
        private final PatternRestClient patternRestClient;
        private final Scenario scenario;
        private final ScenarioCreatorCallback callback;

        private GetScenarioCreatorTask(PatternRestClient patternRestClient, Scenario scenario, ScenarioCreatorCallback callback) {
            this.patternRestClient = patternRestClient;
            this.scenario = scenario;
            this.callback = callback;
        }

        @Override
        protected User doInBackground(Void... voids) {
            try {
                Response<RemoteUser> remoteUserResponse = patternRestClient.getUserWithUrl(scenario.getCreatorLink()).execute();
                if (remoteUserResponse.isSuccessful()) {
                    RemoteUser remoteUser = remoteUserResponse.body();
                    return new User(remoteUser.getSelf().getHref(), remoteUser.getUsername());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            callback.onCreatorFound(user);
        }
    }


    @Override
    public void getScenarioWithUri(@NonNull String uri, @NonNull GetScenarioCallback callback) {
        GetScenarioWithUriTask task = new GetScenarioWithUriTask(uri, patternRestClient, callback);
        task.execute();
    }

    private static class GetScenarioWithUriTask extends AsyncTask<Void, Void, Scenario> {

        String uri;
        GetScenarioCallback callback;
        PatternRestClient restClient;

        GetScenarioWithUriTask(String uri, PatternRestClient restclient, GetScenarioCallback callback) {
            this.uri = uri;
            this.restClient = restclient;
            this.callback = callback;
        }


        @Override
        protected Scenario doInBackground(Void... voids) {
            try {
                Response<Challenge> response = restClient.getChallangeWithUrl(uri).execute();
                if (response.isSuccessful() && response.body() != null) {
                    Scenario scenario = ChallengeScenarioMapper.map(response.body());
                    addUserInfoToScenario(scenario, restClient);
                    addStrategiesToScenario(scenario, response.body().getStrategies().getHref(), restClient);
                    return scenario;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null)
                callback.onScenarioLoaded(scenario);
            else
                callback.onDataNotAvailable();
        }

    }


    private static void addUserInfoToScenario(Scenario scenario, PatternRestClient restClient) throws IOException {
        Response<RemoteUser> userResponse = restClient.getUserWithUrl(scenario.getCreatorLink()).execute();
        if (userResponse.isSuccessful()) {
            RemoteUser remoteUser = userResponse.body();
            scenario.setUsername(remoteUser.getUsername());
        }
    }


    private static void addStrategiesToScenario(Scenario scenario, String strategyHref, PatternRestClient restClient) throws IOException {

        Response<StrategyList> remoteStrategyResponse = restClient.getStrategiesWithUrl(strategyHref).execute();
        if (remoteStrategyResponse.isSuccessful() && remoteStrategyResponse.body() != null && remoteStrategyResponse.body().getStrategies() != null) {
            List<RemoteStrategy> remoteStrategyList = remoteStrategyResponse.body().getStrategies();
            List<Strategy> strategyList = new ArrayList<>();


            for (RemoteStrategy remoteStrategy : remoteStrategyList) {
                Strategy strategy = StrategyMapper.map(remoteStrategy);
                addCreatorToStrategy(restClient, remoteStrategy, strategy);
                addPatternsToStrategy(restClient, remoteStrategy, strategy);
                strategyList.add(strategy);
            }
            scenario.setStrategies(strategyList);
        }
    }

    private static void addPatternsToStrategy(PatternRestClient restClient, RemoteStrategy remoteStrategy, Strategy strategy) throws IOException {
        Response<PatternList> remotePatternRespone = restClient.getPatternsWithUrl(remoteStrategy.getPatterns().getHref()).execute();
        if (remotePatternRespone.isSuccessful()) {
            List<RemotePattern> remotePatternList = remotePatternRespone.body().getPatterns();
            strategy.setFearlessPattern(PatternMapper.map(remotePatternList));
        }
    }

    private static void addCreatorToStrategy(PatternRestClient restClient, RemoteStrategy remoteStrategy, Strategy strategy) throws IOException {
        Response<RemoteUser> remoteUserResponse = restClient.getUserWithUrl(remoteStrategy.getCreator().getHref()).execute();
        if (remoteUserResponse.isSuccessful()) {
            RemoteUser remoteUser = remoteUserResponse.body();
            strategy.setUsername(remoteUser.getUsername());
            strategy.setCreatorLink(remoteUser.getSelf().getHref());
        }
    }


    @Override
    public void deleteScenario(@NonNull Scenario scenario, ScenarioDataSource.DeleteScenarioCallback callback) {
        DeleteScenarioTask task = new DeleteScenarioTask(patternRestClient, scenario, callback);
        task.execute();
    }

    private static class DeleteScenarioTask extends AsyncTask<Void, Void, Boolean> {
        private PatternRestClient patternRestClient;
        private Scenario scenario;
        private DeleteScenarioCallback callback;

        private DeleteScenarioTask(PatternRestClient patternRestClient, Scenario scenario, DeleteScenarioCallback callback) {
            this.patternRestClient = patternRestClient;
            this.scenario = scenario;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                return patternRestClient.deleteViaUrl(scenario.getSelfLink()).execute().isSuccessful();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean successful) {
            if (successful)
                callback.onSucess();
            else
                callback.onFailure();
        }
    }

}
