package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

import java.util.Objects;

public class GetCurrentUserObserver implements Observer<User> {


    public interface UserFound {
        void onUserFound(User user);
    }


    private AppCompatActivity owner;
    private UserViewModel userViewModel;
    private UserFound userFound;
    private LayoutInflater inflater;
    private View parent;

    public GetCurrentUserObserver(AppCompatActivity owner, UserViewModel userViewModel, LayoutInflater inflater, View parent, UserFound userFound) {
        this.owner = owner;
        this.userViewModel = userViewModel;
        this.userFound = userFound;
        this.inflater = inflater;
        this.parent = parent;
    }

    @Override
    public void onChanged(@Nullable User user) {
        if (user != null && user.getSelfLink() != null && !user.getSelfLink().isEmpty()) {
            userFound.onUserFound(user);
        } else {
            new RegisterPopup(
                    Objects.requireNonNull(inflater),
                    userViewModel,
                    owner,
                    new Observer<User>() {
                        @Override
                        public void onChanged(@Nullable User user) {
                            userFound.onUserFound(user);
                        }
                    }).showAtLocation(parent, Gravity.CENTER, 0, 0);
            Keyboard.showKeyboard(owner);
        }
    }

}
