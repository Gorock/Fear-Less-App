package com.fearless.prototype.fearless_change_cards.data.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Favorite {

    @PrimaryKey
    @NonNull
    String resourceUri;

    public Favorite(@NonNull String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(@NonNull String resourceUri) {
        this.resourceUri = resourceUri;
    }


}
