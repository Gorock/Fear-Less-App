package com.fearless.prototype.fearless_change_cards.activities.library;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;

import java.util.List;

public interface PatternShower {

    void showPatterns(List<FearlessPattern> patterns);
}
