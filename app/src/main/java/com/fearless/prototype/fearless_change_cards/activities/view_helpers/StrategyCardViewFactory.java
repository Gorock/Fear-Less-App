package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.StrategiesViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

public class StrategyCardViewFactory {


    private final LayoutInflater layoutInflater;
    private final StrategiesViewModel strategiesViewModel;
    private final FavoriteViewModel favoriteViewModel;
    private final UserViewModel userViewModel;
    private final LifecycleOwner owner;
    private final ViewGroup viewGroup;


    public StrategyCardViewFactory(Context context, LifecycleOwner owner,
                                   StrategiesViewModel strategiesViewModel,
                                   FavoriteViewModel favoriteViewModel,
                                   UserViewModel userViewModel, ViewGroup viewGroup) {

        this.strategiesViewModel = strategiesViewModel;
        this.favoriteViewModel = favoriteViewModel;
        this.userViewModel = userViewModel;
        this.owner = owner;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        this.viewGroup = viewGroup;

    }

    public View createStrategyCard(final Strategy strategy) {
        final CardView strategyCard = (CardView) layoutInflater.inflate(R.layout.template_strategies, viewGroup, false);

        ViewSetter viewSetter = new ViewSetter(strategyCard);
        viewSetter.setText(R.id.strategyTitle, strategy.getTitle());
        viewSetter.setText(R.id.strategy_description, strategy.getDescription());
        viewSetter.setText(R.id.strategyUsername, strategy.getUsername());

        final CheckBox likeButton = strategyCard.findViewById(R.id.strategyLike);

        likeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                strategy.setFavorite(isChecked);
                if (isChecked)
                    favoriteViewModel.addFavorite(strategy.getSelfLink());
                else
                    favoriteViewModel.removeFavorite(strategy.getSelfLink());

            }
        });


        favoriteViewModel.queryFavorite(strategy.getSelfLink()).observe(owner, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                likeButton.setChecked(aBoolean);
            }
        });

        userViewModel.queryRegisteredUser().observe(owner, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null && strategy.getCreatorLink().equals(user.getSelfLink())) {
                    ImageButton button = strategyCard.findViewById(R.id.delete_strategy_button);
                    button.setVisibility(View.VISIBLE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("DELETE", strategy.getSelfLink());
                            strategiesViewModel.deleteStrategy(strategy).observe(owner, new Observer<Boolean>() {
                                @Override
                                public void onChanged(@Nullable Boolean deletionSucessful) {
                                    if (deletionSucessful != null && deletionSucessful) {
                                        ((ViewGroup) strategyCard.getParent()).removeView(strategyCard);
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });


        StringBuilder builder = new StringBuilder();
        for (FearlessPattern pattern : strategy.getFearlessPattern()) {
            builder.append(" -> ");
            builder.append(pattern.getName());
        }

        viewSetter.setText(R.id.steps_taken_text, builder.toString());
        viewGroup.addView(strategyCard);
        return strategyCard;
    }
}


