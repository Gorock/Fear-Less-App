package com.fearless.prototype.fearless_change_cards.data.source.remote;

import android.os.AsyncTask;

import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.UserDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.io.IOException;

import retrofit2.Response;

public class RemoteUserDataSource {


    private final PatternRestClient patternRestClient;

    public RemoteUserDataSource() {
        this.patternRestClient = Injection.providePatternRestClient();

    }


    public void registerUser(RemoteUser newUser, UserDataSource.GetUserCallback callback) {
        RegisterUserTask task = new RegisterUserTask(newUser, patternRestClient, callback);
        task.execute();
    }

    private static class RegisterUserTask extends AsyncTask<Void, Void, User> {

        private final UserDataSource.GetUserCallback callback;
        private final RemoteUser newUser;
        private final PatternRestClient patternRestClient;

        RegisterUserTask(RemoteUser newUser, PatternRestClient patternRestClient, UserDataSource.GetUserCallback callback) {
            this.newUser = newUser;
            this.callback = callback;
            this.patternRestClient = patternRestClient;
        }

        @Override
        protected User doInBackground(Void... voids) {
            try {
                Response<RemoteUser> remoteUserResponse = patternRestClient.createUser(newUser).execute();
                if (remoteUserResponse.isSuccessful()) {
                    RemoteUser remoteUser = remoteUserResponse.body();
                    return new User(remoteUser.getSelf().getHref(), remoteUser.getUsername());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null) {
                callback.onUserLoaded(user);
                Injection.getInstance().setCurrentUser(user);
            } else {
                callback.onDataNotAvailable();
            }
        }
    }
}



