package com.fearless.prototype.fearless_change_cards.activities;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.Space;
import android.widget.TextView;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.listeners.OnCardClickListener;
import com.fearless.prototype.fearless_change_cards.view_models.CardOfDayViewModel;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CardOfDayViewModel cardOfDayViewModel = ViewModelProviders.of(this).get(CardOfDayViewModel.class);


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.card_of_the_day);

        cardOfDayViewModel.getCardOfDay().observe(this, new Observer<FearlessPattern>() {
            @Override
            public void onChanged(@Nullable FearlessPattern pattern) {
                showCardOfDay(pattern);
            }
        });

    }

    public void showCardOfDay(FearlessPattern pattern) {
        FrameLayout parent = findViewById(R.id.card_of_day_place);
        CardView card = createCardViewFromPattern(pattern);
        card.setOnClickListener(new OnCardClickListener(this, pattern));
        parent.addView(card);
        Space space = new Space(MainActivity.this);
        space.setMinimumWidth(100);
        parent.addView(space);
    }

    @NonNull
    @SuppressLint("InflateParams")
    private CardView createCardViewFromPattern(FearlessPattern pattern) {
        LayoutInflater layoutInflater = getLayoutInflater();
        FrameLayout parent = findViewById(R.id.card_of_day_place);
        CardView card = (CardView) layoutInflater.inflate(R.layout.template_card_of_day, parent, false);

        TextView title = card.findViewById(R.id.pattern_title);
        if (pattern != null)
            title.setText(pattern.getName());

        return card;
    }


}

