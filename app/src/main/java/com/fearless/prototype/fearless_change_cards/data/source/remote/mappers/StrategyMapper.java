package com.fearless.prototype.fearless_change_cards.data.source.remote.mappers;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteStrategy;

import java.util.ArrayList;
import java.util.List;

public class StrategyMapper {


    private StrategyMapper() {

    }


    public static Strategy map(RemoteStrategy remoteStrategy) {
        Strategy mappedStrategy = new Strategy(
                remoteStrategy.getTitle(),
                remoteStrategy.getDescription()
        );
        mappedStrategy.setSelfLink(remoteStrategy.getSelf().getHref());
        mappedStrategy.setCreatorLink(remoteStrategy.getCreator().getHref());
        mappedStrategy.setChallangeLink(remoteStrategy.getChallenge().getHref());
        return mappedStrategy;
    }

    public static List<Strategy> map(List<RemoteStrategy> remotePatterns) {
        List<Strategy> strategyList = new ArrayList<>();

        for (RemoteStrategy remote :
                remotePatterns) {
            strategyList.add(StrategyMapper.map(remote));
        }
        return strategyList;
    }

    public static RemoteStrategy map(Strategy strategy) {
        RemoteStrategy remoteStrategy = new RemoteStrategy();
        remoteStrategy.setTitle(strategy.getTitle());
        remoteStrategy.setDescription(strategy.getDescription());
        return remoteStrategy;

    }
}
