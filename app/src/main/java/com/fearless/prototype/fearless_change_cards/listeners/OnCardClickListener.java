package com.fearless.prototype.fearless_change_cards.listeners;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fearless.prototype.fearless_change_cards.activities.library.SinglePatternDetailActivity;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;

import static android.support.v4.content.ContextCompat.startActivity;

public class OnCardClickListener implements View.OnClickListener {

    private android.content.Context packageContext;
    private FearlessPattern pattern;

    public OnCardClickListener(android.content.Context context, FearlessPattern pattern) {
        packageContext = context;
        this.pattern = pattern;
    }


    @Override
    public void onClick(View view) {
        Intent myIntent = new Intent(packageContext, SinglePatternDetailActivity.class);
        myIntent.putExtra("patternId", pattern.getId());
        myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(packageContext, myIntent, Bundle.EMPTY);
    }
}
