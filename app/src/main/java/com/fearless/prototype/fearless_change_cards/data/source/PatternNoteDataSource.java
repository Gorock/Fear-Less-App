package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;

import java.util.List;

public interface PatternNoteDataSource {


    interface GetNotesCallback {

        void onNotesLoaded(List<PatternNote> patternNotes);

        void onDataNotAvailable();

    }

    void getNoteWithPatternId(long patternId, @NonNull GetNotesCallback callback);

    void saveNote(@NonNull PatternNote patternNote, @NonNull GetNotesCallback callback);

    void removeNote(PatternNote patternNote, GetNotesCallback callback);

    void refreshNote();

}
