package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;

import java.util.List;

public interface ScenarioDataSource {


    interface GetScenariosCallback {

        void onScenariosLoaded(List<Scenario> scenarios);

        void onDataNotAvailable();

    }

    interface ScenarioCreatorCallback {
        void onCreatorFound(User user);
    }

    interface DeleteScenarioCallback {

        void onSucess();

        void onFailure();

    }

    interface GetScenarioCallback {

        void onScenarioLoaded(Scenario scenario);

        void onDataNotAvailable();
    }


    void getScenarios(@NonNull ScenarioDataSource.GetScenariosCallback callback);

    void getScenario(@NonNull String scenarioId, @NonNull ScenarioDataSource.GetScenarioCallback callback);

    void getScenarioWithUri(@NonNull String uri, @NonNull ScenarioDataSource.GetScenarioCallback callback);

    void saveScenario(@NonNull Scenario scenario, @NonNull ScenarioDataSource.GetScenarioCallback callback);

    void insertScenario(@NonNull Scenario scenario, @NonNull ScenarioDataSource.GetScenarioCallback callback);

    void getScenarioCreator(@NonNull Scenario scenario, @NonNull ScenarioCreatorCallback callback);

    void deleteScenario(@NonNull Scenario scenario, @NonNull DeleteScenarioCallback callback);

}
