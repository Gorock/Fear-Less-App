package com.fearless.prototype.fearless_change_cards.data.source.local;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

@Dao
public interface FearLessDao {

    @Query("Select * from FearlessPattern")
    List<FearlessPattern> getPatterns();


    @Query("Select * from FearlessPattern where id = :patternId")
    FearlessPattern getPattern(long patternId);

    @Query("Select distinct FearlessPattern.* from FearlessPattern, fearless_tag_join " +
            "where " +
            "fearless_tag_join.patternId = FearlessPattern.id " +
            "and solution like :searchTerm  " +
            "or description like :searchTerm " +
            "or problem like :searchTerm " +
            "or name like :searchTerm " +
            "or tagName like :searchTerm")
    List<FearlessPattern> searchPattern(String searchTerm);

    @Query("SELECT * FROM Tag INNER JOIN fearless_tag_join ON" +
            "(Tag.name = fearless_tag_join.tagName) WHERE " +
            "fearless_tag_join.patternId=:patternId")
    List<Tag> getTagsofPattern(final long patternId);

    @Insert
    void insertPatternList(List<FearlessPattern> patterns);

    @Update
    void updatePattern(FearlessPattern pattern);

}
