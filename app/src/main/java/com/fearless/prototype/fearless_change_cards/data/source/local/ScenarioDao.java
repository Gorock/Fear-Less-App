package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;

import java.util.List;

@Dao
public interface ScenarioDao {


    @Query("Select * from Scenario")
    List<Scenario> getScenarios();

    @Query("Select * from Scenario where scenarioId = :scenarioId")
    Scenario getScenario(String scenarioId);

    @Query("Select * from Scenario where selfLink = :uri")
    Scenario getScenarioWithUri(String uri);


    @Insert
    void insertScenarioList(List<Scenario> scenarios);

    @Insert
    long insert(Scenario scenario);

    @Update
    void updateScenario(Scenario scenario);
}