package com.fearless.prototype.fearless_change_cards.activities.view_helpers;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.MainActivity;
import com.fearless.prototype.fearless_change_cards.activities.ProfileActivity;
import com.fearless.prototype.fearless_change_cards.activities.library.GridViewActivity;
import com.fearless.prototype.fearless_change_cards.activities.scenario.ScenariosActivity;

import java.lang.reflect.Field;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomBarFragment extends Fragment {


    public BottomBarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_bottom_bar, container, false);

        addClickSwitchToActivity(v.findViewById(R.id.scenario_button), ScenariosActivity.class);
        addClickSwitchToActivity(v.findViewById(R.id.pattern_button), GridViewActivity.class);
        addClickSwitchToActivity(v.findViewById(R.id.profile_btn), ProfileActivity.class);
        addClickSwitchToActivity(v.findViewById(R.id.card_of_the_day), MainActivity.class);


        removeShiftMode((BottomNavigationView) v.findViewById(R.id.bottomNavigationView));
        return v;
    }


    private void addClickSwitchToActivity(View view, final Class<?> activity) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(BottomBarFragment.this.getContext(), activity);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(myIntent);
            }
        });
    }


    public static void removeShiftMode(BottomNavigationView v) {
        //this will remove shift mode for bottom navigation view
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) v.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }

        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }
}


