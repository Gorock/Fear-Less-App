package com.fearless.prototype.fearless_change_cards.data.source.remote.mappers;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemotePattern;

import java.util.ArrayList;
import java.util.List;

public class PatternMapper {

    private PatternMapper() {

    }


    public static FearlessPattern map(RemotePattern remotePattern, Long id) {
        FearlessPattern mappedPattern = new FearlessPattern(
                remotePattern.getTitle(),
                remotePattern.getDescription(),
                remotePattern.getProblem(),
                remotePattern.getSolution()
        );
        mappedPattern.setId(id);
        mappedPattern.setSelfLink(remotePattern.getSelf().getHref());
        return mappedPattern;
    }

    public static List<FearlessPattern> map(List<RemotePattern> remotePatterns) {
        List<FearlessPattern> fearlessPatternList = new ArrayList<>();
        for (RemotePattern remote :
                remotePatterns) {
            fearlessPatternList.add(PatternMapper.map(remote, 0L)); // use 0 Id because list could be just a Part
        }
        return fearlessPatternList;
    }
}
