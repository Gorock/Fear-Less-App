package com.fearless.prototype.fearless_change_cards.activities.library;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.Keyboard;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;
import com.fearless.prototype.fearless_change_cards.listeners.OnSwipeChangePattern;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.PatternNoteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.PatternViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.TagsViewModel;

import java.util.List;
import java.util.Objects;

public class SinglePatternDetailActivity extends AppCompatActivity {


    private PatternViewModel patternViewModel;
    private PatternNoteViewModel patternNoteViewModel;
    private FavoriteViewModel favoriteViewModel;
    private TagsViewModel tagsViewModel;

    private PopupWindow addTagPopWindow;

    private CardView addTagCard;

    private FearlessPattern pattern;

    private PopupWindow addNotePopup;
    private FrameLayout patternCardPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libary_pattern_detail);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        this.patternCardPlace = findViewById(R.id.SingleCard);
        this.addTagCard = findViewById(R.id.card_add_tag);
        createAddTagPopupWindow();

        Intent intent = getIntent();
        patternNoteViewModel = ViewModelProviders.of(this).get(PatternNoteViewModel.class);
        tagsViewModel = ViewModelProviders.of(this).get(TagsViewModel.class);
        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);

        patternViewModel = ViewModelProviders.of(this).get(PatternViewModel.class);
        final Long patternId = (Long) Objects.requireNonNull(intent.getExtras()).get("patternId");
        getPattern(patternId);

        BottomNavigationView topView = findViewById(R.id.top_view_selection);
        LibaryTopNavigation.createTopNavigation(topView, this, R.id.DetailButton);

        final View nextBtn = findViewById(R.id.next_pattern_button);
        final View previousBtn = findViewById(R.id.previous_pattern_button);
        nextBtn.setOnClickListener(createSwitchPatternListener(+1));
        previousBtn.setOnClickListener(createSwitchPatternListener(-1));
    }

    private View.OnClickListener createSwitchPatternListener(final long step) {
        return (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPattern(pattern.getId() + step);
            }
        });
    }


    private void createAddTagPopupWindow() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.popup_insert_tag, null);

        this.addTagPopWindow = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);


        View.OnClickListener showTagPopup = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTagPopWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                Keyboard.showKeyboard(SinglePatternDetailActivity.this);
            }
        };
        this.addTagCard.setOnClickListener(showTagPopup);
        Button button = findViewById(R.id.btn_addTag);
        button.setOnClickListener(showTagPopup);

        final Button addTagButton = this.addTagPopWindow.getContentView().findViewById(R.id.add_new_tag_button);
        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTagPopWindow.dismiss();
                EditText textView = v.getRootView().findViewById(R.id.insert_tag_text);

                tagsViewModel.addTagToPatternWithId(pattern.getId(),
                        textView.getText().toString());

            }
        });
    }


    private void createAddNotePopupWindow() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.popup_insert_note, null);
        this.addNotePopup = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);
        final Button saveNoteButton = this.addNotePopup.getContentView().findViewById(R.id.popup_add_note);
        saveNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotePopup.dismiss();
                EditText noteText = v.getRootView().findViewById(R.id.popup_note_text);
                patternNoteViewModel.saveNote(new PatternNote(noteText.getText().toString(), pattern.getId()));

            }
        });

        ImageButton addNoteBtn = findViewById(R.id.card_add_note_btn);
        addNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotePopup.showAtLocation(v, Gravity.CENTER, 0, 0);
                Keyboard.showKeyboard(SinglePatternDetailActivity.this);
            }
        });


    }


    @SuppressLint("ClickableViewAccessibility")
    public void showPattern(final FearlessPattern pattern) {
        this.pattern = pattern;

        this.patternCardPlace.removeAllViews();
        patternCardPlace.setOnTouchListener(new OnSwipeChangePattern(this) {

            @Override
            public boolean onDownTouch(MotionEvent motionEvent) {
                showPatternBack(pattern);
                return false;
            }

            @Override
            public void onSwipeLeft() {
                getPattern(pattern.getId() + 1L);
            }

            @Override
            public void onSwipeRight() {
                getPattern(pattern.getId() - 1L);
            }
        });

        addPattern(pattern);
        createAddNotePopupWindow();
    }

    private void addPattern(final FearlessPattern pattern) {
        int resource = R.layout.template_pattern_card_detail;
        LayoutInflater layoutInflater = getLayoutInflater();


        CardView card = (CardView) layoutInflater.inflate(resource, patternCardPlace, false);
        ViewAttributeSetter setter = new ViewAttributeSetter(card);

        setter.setTextViewTextWithViewId(pattern.getName(), R.id.pattern_title);
        setter.setTextViewTextWithViewId(String.valueOf(pattern.getId()), R.id.patternId);
        setter.setTextViewTextWithViewId(pattern.getDescription(), R.id.description);

        setter.setCheckboxWithViewID(pattern, R.id.favoriteSwitch);
        final CheckBox favoriteSwitch = card.findViewById(R.id.favoriteSwitch);
        favoriteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean newFavoriteStatus) {
                if (newFavoriteStatus)
                    favoriteViewModel.addFavorite(pattern.getSelfLink());
                else
                    favoriteViewModel.removeFavorite(pattern.getSelfLink());

            }
        });

        patternCardPlace.addView(card);
    }


    @SuppressLint("ClickableViewAccessibility")
    public void showPatternBack(final FearlessPattern pattern) {
        this.pattern = pattern;

        this.patternCardPlace.removeAllViews();
        addBackPattern(pattern);

        patternCardPlace.setOnTouchListener(new OnSwipeChangePattern(this) {
            @Override
            public boolean onDownTouch(MotionEvent e) {
                showPattern(pattern);
                return false;
            }

            @Override
            public void onSwipeLeft() {
                getPattern(pattern.getId() + 1L);
            }

            @Override
            public void onSwipeRight() {
                getPattern(pattern.getId() - 1L);
            }
        });
    }

    private void addBackPattern(final FearlessPattern pattern) {
        int resource = R.layout.template_pattern_card_back;
        LayoutInflater layoutInflater = getLayoutInflater();

        CardView card = (CardView) layoutInflater.inflate(resource, patternCardPlace, false);

        ViewAttributeSetter setter = new ViewAttributeSetter(card);

        setter.setTextViewTextWithViewId(pattern.getName(), R.id.pattern_title);
        setter.setTextViewTextWithViewId(pattern.getSolution(), R.id.pattern_solution_text);
        setter.setTextViewTextWithViewId(pattern.getProblem(), R.id.pattern_problem_text);
        setter.setTextViewTextWithViewId("", R.id.pattern_but_text);

        setter.setCheckboxWithViewID(pattern, R.id.favoriteSwitch);
        CheckBox favoriteSwitch = card.findViewById(R.id.favoriteSwitch);
        favoriteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean newFavoriteStatus) {
                if (newFavoriteStatus)
                    favoriteViewModel.addFavorite(pattern.getSelfLink());
                else
                    favoriteViewModel.removeFavorite(pattern.getSelfLink());
            }
        });

        patternCardPlace.addView(card);

    }

    private class ViewAttributeSetter {
        private final View root;

        private ViewAttributeSetter(View root) {
            this.root = root;
        }

        private void setTextViewTextWithViewId(String text, int textViewID) {
            TextView textView = root.findViewById(textViewID);
            if (textView != null) {
                textView.setText(text);
            }
        }

        private void setCheckboxWithViewID(FearlessPattern pattern, int checkBoxViewID) {
            final CheckBox checkBox = root.findViewById(checkBoxViewID);
            if (checkBox != null) {
                favoriteViewModel.queryFavorite(pattern.getSelfLink())
                        .observe(SinglePatternDetailActivity.this, new Observer<Boolean>() {
                            @Override
                            public void onChanged(@Nullable Boolean aBoolean) {
                                checkBox.setChecked(aBoolean);
                            }
                        });
            }
        }
    }


    private void getPattern(long patternId) {
        patternViewModel.getPatternWithId(patternId).observe(this, new Observer<FearlessPattern>() {
            @Override
            public void onChanged(@Nullable FearlessPattern pattern) {
                if (pattern == null) {
                    Toast noPatternMessage = Toast.makeText(SinglePatternDetailActivity.this, "No Pattern Found", Toast.LENGTH_LONG);
                    noPatternMessage.show();
                    return;
                }
                showPattern(pattern);
                createAddNotePopupWindow();
                tagsViewModel.getTagsForPatternId(pattern.getId()).observe(SinglePatternDetailActivity.this,
                        new Observer<List<Tag>>() {
                            @Override
                            public void onChanged(@Nullable List<Tag> tags) {
                                if (tags != null)
                                    showTags(tags);
                            }
                        });

                patternNoteViewModel.getNotesForPatternWithId(pattern.getId()).observe(SinglePatternDetailActivity.this,
                        new Observer<List<PatternNote>>() {
                            @Override
                            public void onChanged(@Nullable List<PatternNote> patternNotes) {
                                if (patternNotes != null)
                                    showNotes(patternNotes);
                            }
                        });


            }
        });
    }


    public void showTags(List<Tag> tags) {
        ViewGroup parent = findViewById(R.id.tagLayout);
        parent.removeAllViews();
        parent.addView(this.addTagCard);
        for (final Tag tag : tags) {
            CardView.LayoutParams layoutParams = new CardView.LayoutParams(
                    CardView.LayoutParams.WRAP_CONTENT, CardView.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(5, 5, 5, 5);
            CardView card = new CardView(this);
            card.setLayoutParams(layoutParams);
            TextView textView = new TextView(this);
            textView.setText(tag.getName());
            card.addView(textView);
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tagsViewModel.removeTagFromPatternWithId(pattern.getId(), tag)
                            .observe(SinglePatternDetailActivity.this, new Observer<List<Tag>>() {
                                @Override
                                public void onChanged(@Nullable List<Tag> tags) {
                                    showTags(tags);
                                }
                            });

                }
            });
            parent.addView(card);
        }
    }

    public void showNotes(List<PatternNote> notes) {
        LinearLayout layout = findViewById(R.id.linearLayoutForNotes);
        if (layout.getChildCount() > 1)
            layout.removeViews(1, layout.getChildCount() - 1);
        LayoutInflater layoutInflater = getLayoutInflater();

        for (final PatternNote patternNote : notes) {
            CardView patternNoteTemplate = (CardView) layoutInflater.inflate(R.layout.template_pattern_note, layout, false);
            new ViewAttributeSetter(patternNoteTemplate).setTextViewTextWithViewId(patternNote.getText(), R.id.noteText);
            View deleteNoteButton = patternNoteTemplate.findViewById(R.id.delete_note_button);
            deleteNoteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    patternNoteViewModel.removeNote(patternNote);
                }
            });
            layout.addView(patternNoteTemplate);
        }
    }


}
