package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;

import java.util.List;

public class StrategyRepository implements StrategyDataSource {


    private static StrategyRepository instance;
    private static StrategyDataSource strategyDataSource;
    private static StrategyDataSource firstDataSource;

    private StrategyRepository() {
    }

    public static StrategyRepository getINSTANCE(StrategyDataSource dataSource, StrategyDataSource firstDataSource) {

        if (instance == null) {
            instance = new StrategyRepository();
            StrategyRepository.strategyDataSource = dataSource;
            StrategyRepository.firstDataSource = firstDataSource;
        }
        return instance;
    }

    public static StrategyRepository getINSTANCE(StrategyDataSource dataSource) {
        return getINSTANCE(dataSource, dataSource);
    }

    @Override
    public void deleteStrategy(Strategy strategy, OnStragegiesDeletetionCallback deletetionCallback) {
        firstDataSource.deleteStrategy(strategy, deletetionCallback);
    }

    @Override
    public void getStrategies(@NonNull final GetStrategiesCallback callback) {
        firstDataSource.getStrategies(new GetStrategiesCallback() {
            @Override
            public void onStrategiesLoaded(List<Strategy> strategies) {
                callback.onStrategiesLoaded(strategies);
            }

            @Override
            public void onDataNotAvailable() {
                strategyDataSource.getStrategies(callback);
            }
        });
    }

    @Override
    public void getStrategy(@NonNull String scenarioId, @NonNull GetStrategyCallback callback) {
        throw new UnsupportedOperationException("Not Implemented Yet");
    }

    @Override
    public void saveStrategy(@NonNull final Strategy strategy, @NonNull final GetStrategyCallback callback) {
        firstDataSource.saveStrategy(strategy, new GetStrategyCallback() {
            @Override
            public void onStrategyLoaded(Strategy strategy) {
                callback.onStrategyLoaded(strategy);
            }

            @Override
            public void onDataNotAvailable() {
                strategyDataSource.saveStrategy(strategy, callback);

            }
        });
    }

}
