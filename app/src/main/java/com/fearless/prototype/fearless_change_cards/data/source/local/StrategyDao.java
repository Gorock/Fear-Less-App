package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;

import java.util.List;

@Dao
public interface StrategyDao {

    @Query("SELECT * from Strategy")
    List<Strategy> getStrategies();

    @Query("Select * from Strategy where id = :strategyId")
    Strategy getStrategyWithId(Long strategyId);


    @Insert
    void insert(Strategy strategy);

    @Update
    void save(Strategy strategy);
}
