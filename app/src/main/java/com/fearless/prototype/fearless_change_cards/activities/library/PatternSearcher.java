package com.fearless.prototype.fearless_change_cards.activities.library;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.view_models.SearchPatternsViewModel;

import java.util.List;

public class PatternSearcher {

    private PatternSearcher() {
    }

    public static void makePatternSearcher(final PatternShower patternShower, SearchView searchView, final SearchPatternsViewModel searchPatternsViewModel, final LifecycleOwner owner) {

        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchPatternsViewModel.searchPatterns(query).observe(owner,
                        new Observer<List<FearlessPattern>>() {
                            @Override
                            public void onChanged(@Nullable List<FearlessPattern> patterns) {
                                patternShower.showPatterns(patterns);
                            }
                        }

                );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }


}
