package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

public class CardOfDayViewModel extends AndroidViewModel {


    private PatternDataSource patternDataSource;
    private MutableLiveData<FearlessPattern> patternMutableLiveData;
    private static final int SECONDS_IN_A_DAY = 86400;
    private static final int NUMBER_OF_PATTERNS = 53;


    public CardOfDayViewModel(@NonNull Application application) {
        super(application);
        this.patternDataSource = Injection.providePatternRepository(application.getApplicationContext());
    }

    public LiveData<FearlessPattern> getCardOfDay() {

        long unixTime = System.currentTimeMillis() / 1000L;
        long patternId = (unixTime / SECONDS_IN_A_DAY) % NUMBER_OF_PATTERNS;

        if (patternMutableLiveData == null)
            patternMutableLiveData = new MutableLiveData<>();

        this.patternDataSource.getPattern(patternId, new PatternDataSource.GetPatternCallback() {
            @Override
            public void onPatternLoaded(FearlessPattern pattern) {
                patternMutableLiveData.setValue(pattern);
            }

            @Override
            public void onDataNotAvailable() {
//                throw new UnsupportedOperationException();
            }
        });

        return patternMutableLiveData;
    }
}
