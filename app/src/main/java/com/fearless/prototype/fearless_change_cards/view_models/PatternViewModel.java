package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Favorite;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.FavoriteLocalDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class PatternViewModel extends AndroidViewModel {

    private PatternDataSource patternDataSource;
    private MutableLiveData<List<FearlessPattern>> patternList;


    public PatternViewModel(@NonNull Application application) {
        super(application);
        this.patternDataSource = Injection.providePatternRepository(application.getApplicationContext());
    }


    public LiveData<List<FearlessPattern>> getPatterns() {

        if (patternList == null)
            patternList = new MutableLiveData<>();
        patternDataSource.getPatterns(new PatternDataSource.GetPatternsCallback() {
            @Override
            public void onPatternsLoaded(List<FearlessPattern> patterns) {
                patternList.setValue(patterns);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return patternList;
    }



    public LiveData<FearlessPattern> getPatternWithId(long id) {
        final MutableLiveData<FearlessPattern> patternLiveData = new MutableLiveData<>();
        patternDataSource.getPattern(id, new PatternDataSource.GetPatternCallback() {

            @Override
            public void onPatternLoaded(FearlessPattern pattern) {
                patternLiveData.setValue(pattern);
            }

            @Override
            public void onDataNotAvailable() {
                // when pattern is not Availbe give error message in view so we just pass it through
                patternLiveData.setValue(null);

            }
        });
        return patternLiveData;
    }
}
