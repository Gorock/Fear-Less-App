package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;

import java.util.List;

public interface StrategyDataSource {

    void deleteStrategy(Strategy strategy, OnStragegiesDeletetionCallback deletetionCallback);

    interface OnStragegiesDeletetionCallback {
        void onSuccess();

        void onFailure();
    }


    interface GetStrategiesCallback {

        void onStrategiesLoaded(List<Strategy> strategies);

        void onDataNotAvailable();

    }

    interface GetStrategyCallback {

        void onStrategyLoaded(Strategy strategy);

        void onDataNotAvailable();
    }


    void getStrategies(@NonNull StrategyDataSource.GetStrategiesCallback callback);

    void getStrategy(@NonNull String scenarioId, @NonNull StrategyDataSource.GetStrategyCallback callback);

    void saveStrategy(@NonNull Strategy strategy, @NonNull StrategyDataSource.GetStrategyCallback callback);


}
