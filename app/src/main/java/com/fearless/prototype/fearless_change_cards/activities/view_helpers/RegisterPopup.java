package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

public class RegisterPopup {

    PopupWindow registerUserPopupWindow;

    public RegisterPopup(LayoutInflater inflater, final UserViewModel model,
                         final LifecycleOwner owner, final Observer<User> userObserver) {

        View layout = inflater.inflate(R.layout.popup_register_user, null);

        registerUserPopupWindow = new PopupWindow(layout,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true);

        final Button registerUserButton = registerUserPopupWindow.getContentView().findViewById(R.id.register_user_btn);
        registerUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUserPopupWindow.dismiss();
                EditText usernameText = registerUserPopupWindow.getContentView().findViewById(R.id.username_text);
                EditText passwordText = v.getRootView().findViewById(R.id.password_text);
                model.registerUser(new RemoteUser(usernameText.getText().toString(), passwordText.getText().toString()))
                        .observe(owner, userObserver);
            }
        });
    }

    public void showAtLocation(View parent, int gravity, int x, int y) {
        registerUserPopupWindow.showAtLocation(parent, gravity, x, y);
    }


    public PopupWindow getPopupWindow() {
        return registerUserPopupWindow;
    }


}
