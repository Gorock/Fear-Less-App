package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource;
import com.fearless.prototype.fearless_change_cards.data.model.FearLessPatternTagJoin;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

public class TagLocalDataSource implements TagDataSource {

    private static volatile TagLocalDataSource instance;
    private static volatile TagsDao tagsDao;
    private static volatile FearlessTagsJoinDao fearlessTagsJoinDao;


    public static TagLocalDataSource getInstance(@NonNull TagsDao tagsDao, @NonNull FearlessTagsJoinDao fearlessTagsJoinDao) {
        if (instance == null) {
            synchronized (TagLocalDataSource.class) {
                if (instance == null) {
                    instance = new TagLocalDataSource();
                    TagLocalDataSource.tagsDao = tagsDao;
                    TagLocalDataSource.fearlessTagsJoinDao = fearlessTagsJoinDao;
                }
            }
        }
        return instance;
    }


    @Override
    public void getTags(Long fearlessPatternId, @NonNull GetTagsCallback callback) {
        GetTagsTask tagsTask = new GetTagsTask(callback);
        tagsTask.execute(fearlessPatternId);
    }

    private static class GetTagsTask extends AsyncTask<Long, Void, List<Tag>> {

        private GetTagsCallback callback;

        GetTagsTask(GetTagsCallback callback) {
            this.callback = callback;
        }


        @Override
        protected List<Tag> doInBackground(Long... longs) {
            return fearlessTagsJoinDao.getTagsofPattern(longs[0]);
        }

        @Override
        protected void onPostExecute(List<Tag> tags) {
            this.callback.onTagsLoaded(tags);
        }

    }


    @Override
    public void getTag(@NonNull String tagName, @NonNull GetTagCallback callback) {
        GetTagTask task = new GetTagTask(callback);
        task.execute(tagName);
    }

    private static class GetTagTask extends AsyncTask<String, Void, Tag> {

        private GetTagCallback callback;

        GetTagTask(GetTagCallback callback) {
            this.callback = callback;
        }

        @Override
        protected Tag doInBackground(String... strings) {
            return tagsDao.findTagByName(strings[0]);
        }

        @Override
        protected void onPostExecute(Tag tag) {
            this.callback.onTagLoaded(tag);
        }

    }


    @Override
    public void saveTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback) {
        SaveFearlessTagTask task = new SaveFearlessTagTask(tag, fearlessPatternId, callback);
        task.execute();
    }


    private static class SaveFearlessTagTask extends AsyncTask<Void, Void, List<Tag>> {

        private Tag tag;
        private Long fearlessPatternId;
        private GetTagsCallback callback;

        SaveFearlessTagTask(Tag tag, Long fearlessPatternId, GetTagsCallback callback) {
            this.tag = tag;
            this.fearlessPatternId = fearlessPatternId;
            this.callback = callback;
        }

        @Override
        protected List<Tag> doInBackground(Void... voids) {
            tagsDao.insert(tag);
            Log.d(TagLocalDataSource.class.getSimpleName(), fearlessPatternId + "");
            fearlessTagsJoinDao.insert(new FearLessPatternTagJoin(fearlessPatternId, tag.getName()));
            return fearlessTagsJoinDao.getTagsofPattern(fearlessPatternId);
        }


        @Override
        protected void onPostExecute(List<Tag> tags) {
            this.callback.onTagsLoaded(tags);
        }
    }


    @Override
    public void removeTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback) {
        RemoveTagWithPatternTask task = new RemoveTagWithPatternTask(new FearLessPatternTagJoin(fearlessPatternId, tag.getName()), callback);
        task.execute();
    }


    private static class RemoveTagWithPatternTask extends AsyncTask<Void, Void, List<Tag>> {

        FearLessPatternTagJoin fearLessPatternTagJoin;
        GetTagsCallback callback;

        public RemoveTagWithPatternTask(FearLessPatternTagJoin fearLessPatternTagJoin, GetTagsCallback callback) {
            this.fearLessPatternTagJoin = fearLessPatternTagJoin;
            this.callback = callback;
        }

        @Override
        protected List<Tag> doInBackground(Void... voids) {
            fearlessTagsJoinDao.removeTagsOfPattern(fearLessPatternTagJoin);
            return fearlessTagsJoinDao.getTagsofPattern(fearLessPatternTagJoin.patternId);
        }

        @Override
        protected void onPostExecute(List<Tag> tags) {
            if (tags != null)
                callback.onTagsLoaded(tags);
            else
                callback.onDataNotAvailable();
        }
    }
}

