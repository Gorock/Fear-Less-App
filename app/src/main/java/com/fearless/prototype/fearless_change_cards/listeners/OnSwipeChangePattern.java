package com.fearless.prototype.fearless_change_cards.listeners;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class OnSwipeChangePattern implements View.OnTouchListener {

    private final GestureDetector gestureDetector;

    public OnSwipeChangePattern(Context context) {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view != null)
            view.performClick();
        return gestureDetector.onTouchEvent(motionEvent);
    }


    public boolean onDownTouch(MotionEvent e) {
        throw new UnsupportedOperationException();
    }

    public void onSwipeLeft() {
        throw new UnsupportedOperationException();
    }


    public void onSwipeRight() {
        throw new UnsupportedOperationException();
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_DISTANCE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            onDownTouch(e);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1 == null || e2 == null)
                return false;
            float distanceXAbs = Math.abs(e2.getX() - e1.getX());
            float distanceX = e2.getX() - e1.getX();
            float distanceY = Math.abs(e2.getY() - e1.getY());

            Log.d(this.getClass().getSimpleName(), "On Fling distanceAbs: " + distanceXAbs + " VelocityX: " + velocityX);
            Log.d(this.getClass().getSimpleName(), "On Fling distanceY: " + distanceY);


            if (distanceXAbs > distanceY && distanceXAbs > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0)
                    onSwipeRight();
                else
                    onSwipeLeft();
                return true;
            }
            onDownTouch(e2);
            return false;

        }
    }
}


