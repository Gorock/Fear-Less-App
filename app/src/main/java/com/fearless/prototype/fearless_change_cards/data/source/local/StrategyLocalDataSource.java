package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.source.StrategyDataSource;

import java.util.List;

public class StrategyLocalDataSource implements StrategyDataSource {


    private static volatile StrategyLocalDataSource instance;
    private static volatile StrategyDao strategyDao;

    private StrategyLocalDataSource() {

    }

    public static StrategyLocalDataSource getInstance(@NonNull StrategyDao strategyDao) {
        if (instance == null) {
            synchronized (TagLocalDataSource.class) {
                if (instance == null) {
                    instance = new StrategyLocalDataSource();
                    StrategyLocalDataSource.strategyDao = strategyDao;
                }
            }
        }
        return instance;
    }


    @Override
    public void deleteStrategy(Strategy strategy, OnStragegiesDeletetionCallback deletetionCallback) {
        throw new UnsupportedOperationException("Implement if needed");
    }

    @Override
    public void getStrategies(@NonNull GetStrategiesCallback callback) {
        new GetStrategiesTask(callback).execute();
    }

    private static class GetStrategiesTask extends AsyncTask<Void, Void, List<Strategy>> {

        private GetStrategiesCallback callback;

        GetStrategiesTask(GetStrategiesCallback callback) {
            this.callback = callback;
        }


        @Override
        protected List<Strategy> doInBackground(Void... voids) {
            return strategyDao.getStrategies();
        }

        @Override
        protected void onPostExecute(List<Strategy> strategies) {
            this.callback.onStrategiesLoaded(strategies);
        }

    }


    @Override
    public void getStrategy(@NonNull String scenarioId, @NonNull GetStrategyCallback callback) {
        throw new UnsupportedOperationException("Not implemeted Yet");
    }

    @Override
    public void saveStrategy(@NonNull Strategy strategy, @NonNull GetStrategyCallback callback) {
        StrategySaveTask task = new StrategySaveTask(strategy, callback);
        task.execute();
    }


    private static class StrategySaveTask extends AsyncTask<Void, Void, Strategy> {

        private Strategy strategy;
        private GetStrategyCallback callback;

        StrategySaveTask(Strategy strategy, GetStrategyCallback callback) {
            this.strategy = strategy;
            this.callback = callback;
        }

        @Override
        protected Strategy doInBackground(Void... voids) {
            strategyDao.save(strategy);
            return strategy;
        }


        @Override
        protected void onPostExecute(Strategy strategy) {
            callback.onStrategyLoaded(strategy);
        }
    }
}
