package com.fearless.prototype.fearless_change_cards.activities.strategies;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.scenario.SingleScenarioDetail;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.BottomBar;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.PatternViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.StrategiesViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CreateStrategyActivity extends AppCompatActivity {


    private StrategiesViewModel strategiesViewModel;
    private UserViewModel userViewModel;
    private PatternViewModel patternViewModel;
    private User currentUser;
    private List<FearlessPattern> selectedPatterns;
    private FloatingActionButton addPatternButton;
    private LayoutInflater inflater;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_strategy);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        selectedPatterns = new ArrayList<>();
        inflater = getLayoutInflater();


        createViewModels();


        final PopupWindow selectPatternPopup = createPatternSelectionPopup();
        this.addPatternButton = findViewById(R.id.add_pattern_to_strategy_btn);
        addPatternButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPatternPopup.showAtLocation(addPatternButton, Gravity.CENTER, 0, 0);
            }
        });

        setupBottomNavigationBar();

        final TextView strategyTitle = findViewById(R.id.strategy_title_text);
        final TextView strategyDescription = findViewById(R.id.strategy_description_text);

        Button addSolutionStrategyButton = findViewById(R.id.add_solution_strat_btn);


        userViewModel.queryRegisteredUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null && user.getSelfLink() != null && !user.getSelfLink().isEmpty()) {
                    CreateStrategyActivity.this.currentUser = user;
                }
            }
        });


        Intent intent = getIntent();
        final Long scenarioId = (Long) Objects.requireNonNull(intent.getExtras()).get("scenarioId");
        final String scenarioUri = (String) Objects.requireNonNull(intent.getExtras()).get("scenarioUri");

        addSolutionStrategyButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                if (strategyTitle != null && strategyTitle.getText().length() != 0) {
                    String title = strategyTitle.getText().toString();
                    String description = strategyDescription.getText().toString();
                    Strategy newStrategy = new Strategy(title, description);
                    newStrategy.setChallangeLink(scenarioUri);
                    newStrategy.setCreatorLink(currentUser.getSelfLink());
                    newStrategy.setFearlessPattern(selectedPatterns);
                    strategiesViewModel.saveStrategy(newStrategy)
                            .observe(CreateStrategyActivity.this, new Observer<Strategy>() {
                                @Override
                                public void onChanged(@Nullable Strategy strategy) {

                                    Intent myIntent = new Intent(CreateStrategyActivity.this, SingleScenarioDetail.class);
                                    myIntent.putExtra("scenarioId", scenarioId);
                                    myIntent.putExtra("scenarioUri", scenarioUri);
                                    myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(myIntent, Bundle.EMPTY);


                                }
                            });
                }
            }
        });

    }

    private void setupBottomNavigationBar() {
        BottomNavigationView selectedView = findViewById(R.id.bottomNavigationView);
        selectedView.setSelectedItemId(R.id.scenario_button);
    }

    private PopupWindow createPatternSelectionPopup() {
        View layout = inflater.inflate(R.layout.popup_select_pattern, null);
        final PopupWindow selectPatternToAddPopup = new PopupWindow(layout,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true);

        final ViewGroup selectedPatternsViewGroup = findViewById(R.id.strategy_patterns_list);

        final ViewGroup patternListViewGroup = selectPatternToAddPopup.getContentView().findViewById(R.id.strategy_pattern_selection_list);
        patternViewModel.getPatterns().observe(this, new Observer<List<FearlessPattern>>() {
            @Override
            public void onChanged(@Nullable List<FearlessPattern> patterns) {
                if (patterns == null)
                    return;

                selectedPatternsViewGroup.removeAllViews();
                for (final FearlessPattern pattern :
                        patterns) {
                    final CardView cardView = createCardViewFromPattern(pattern, patternListViewGroup);
                    cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedPatterns.add(pattern);
                            final CardView cardView = createCardViewFromPattern(pattern, selectedPatternsViewGroup);
                            cardView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    selectedPatterns.remove(pattern);
                                    selectedPatternsViewGroup.removeView(cardView);
                                }
                            });


                            selectedPatternsViewGroup.addView(cardView);
                            selectPatternToAddPopup.dismiss();
                        }
                    });
                    patternListViewGroup.addView(cardView);
                }
            }
        });
        return selectPatternToAddPopup;
    }

    private void createViewModels() {
        this.strategiesViewModel = ViewModelProviders.of(this).get(StrategiesViewModel.class);
        this.userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        this.patternViewModel = ViewModelProviders.of(this).get(PatternViewModel.class);
    }


    @NonNull
    @SuppressLint("InflateParams")
    private CardView createCardViewFromPattern(FearlessPattern pattern, ViewGroup parent) {
        assert this.inflater != null;
        CardView card = (CardView) inflater.inflate(R.layout.template_pattern_title_card, parent, false);

        TextView title = card.findViewById(R.id.pattern_title);
        if (pattern != null)
            title.setText(pattern.getName());

        return card;
    }
}
