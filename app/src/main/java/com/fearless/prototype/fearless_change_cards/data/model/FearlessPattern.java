package com.fearless.prototype.fearless_change_cards.data.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Objects;

@Entity(tableName = "FearlessPattern")
public final class FearlessPattern {


    @PrimaryKey(autoGenerate = true)
    private long id;

    private String selfLink;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @Nullable
    @ColumnInfo(name = "description")
    private String description;

    @Nullable
    @ColumnInfo(name = "problem")
    private String problem;

    @Nullable
    @ColumnInfo(name = "solution")
    private String solution;


    @Nullable
    @ColumnInfo(name = "isFavorite")
    private boolean isFavorite;

    public FearlessPattern(@NonNull String name, @Nullable String description, @Nullable String problem, @Nullable String solution) {
        this.name = name;
        this.description = description;
        this.problem = problem;
        this.solution = solution;
        this.isFavorite = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public String getProblem() {
        return problem;
    }

    @Nullable
    public String getSolution() {
        return solution;
    }


    @Nullable
    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(@Nullable boolean favorite) {
        isFavorite = favorite;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public void setProblem(@Nullable String problem) {
        this.problem = problem;
    }

    public void setSolution(@Nullable String solution) {
        this.solution = solution;
    }

    @Override
    public String toString() {
        return "FearlessPattern{" +
                "patternId=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", problem='" + problem + '\'' +
                ", solution='" + solution + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FearlessPattern that = (FearlessPattern) o;
        return id == that.id &&
                isFavorite == that.isFavorite &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(problem, that.problem) &&
                Objects.equals(solution, that.solution);
    }


    public boolean matchSearchTerm(String searchTerm) {
        searchTerm = searchTerm.toLowerCase();
        return name.toLowerCase().contains(searchTerm) ||
                description.toLowerCase().contains(searchTerm) ||
                problem.toLowerCase().contains(searchTerm) ||
                solution.toLowerCase().contains(searchTerm);

    }


    @Override
    public int hashCode() {

        return Objects.hash(id, name, description, problem, solution, isFavorite);
    }
}
