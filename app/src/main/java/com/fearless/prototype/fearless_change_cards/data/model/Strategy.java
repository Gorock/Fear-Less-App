package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

@Entity
public class Strategy {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    private String title;

    private String description;

    private boolean isFavorite;

    private String username;

    private String selfLink;


    private String creatorLink;

    private String challangeLink;

    @Ignore
    private List<FearlessPattern> fearlessPattern;

    public Strategy(@NonNull String title, String description) {
        this.title = title;
        this.description = description;
        this.isFavorite = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getCreatorLink() {
        return creatorLink;
    }

    public void setCreatorLink(String creatorLink) {
        this.creatorLink = creatorLink;
    }

    public String getChallangeLink() {
        return challangeLink;
    }

    public void setChallangeLink(String challangeLink) {
        this.challangeLink = challangeLink;
    }


    public List<FearlessPattern> getFearlessPattern() {
        return fearlessPattern;
    }

    public void setFearlessPattern(List<FearlessPattern> fearlessPattern) {
        this.fearlessPattern = fearlessPattern;
    }
}
