package com.fearless.prototype.fearless_change_cards.data.source.local;


import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Favorite;
import com.fearless.prototype.fearless_change_cards.data.model.FearLessPatternTagJoin;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;
import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;
import com.fearless.prototype.fearless_change_cards.data.model.User;

import java.util.ArrayList;
import java.util.List;

@Database(entities =
        {
                FearlessPattern.class,
                Scenario.class,
                Tag.class,
                FearLessPatternTagJoin.class,
                Strategy.class,
                PatternNote.class,
                Favorite.class,
                User.class
        }, version = 1)
public abstract class PatternDatabase extends RoomDatabase {
    private static PatternDatabase patternDatabase;

    public abstract FearLessDao fearLessPatternDao();

    public abstract ScenarioDao fearLessScenarioDao();

    public abstract TagsDao fearLessTagDao();

    public abstract UserDao fearLessUserDao();

    public abstract FearlessTagsJoinDao fearLessPatternTagJoinDao();

    public abstract FavoriteDao fearlessFavoriteDao();


    public abstract StrategyDao fearLessStrategyDao();


    public abstract PatternNoteDao fearLessPatternNoteDao();

    private static final Object sLock = new Object();


    public static PatternDatabase getINSTANCE(final Context context) {
        synchronized (sLock) {
            if (patternDatabase == null) {
                patternDatabase = Room.databaseBuilder(context.getApplicationContext(),
                        PatternDatabase.class, "Pattern.db").addCallback(
                        new Callback() {
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);

                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        getINSTANCE(context).fearLessPatternDao().insertPatternList(generatePatternData());
                                        getINSTANCE(context).fearLessScenarioDao().insertScenarioList(generateDummyScenarios());
                                        getINSTANCE(context).fearLessTagDao().insertTagList(generateTags());
                                        getINSTANCE(context).fearLessPatternTagJoinDao().insert(new FearLessPatternTagJoin(1L, "hello"));
                                        getINSTANCE(context).fearLessPatternTagJoinDao().insert(new FearLessPatternTagJoin(1L, "awesome"));
                                        getINSTANCE(context).fearLessPatternTagJoinDao().insert(new FearLessPatternTagJoin(2L, "world"));
                                        getINSTANCE(context).fearLessStrategyDao().insert(new Strategy("Hello", "This is A Dummy don't touch it"));
                                        getINSTANCE(context).fearLessPatternNoteDao().addPersonalNote(new PatternNote("hello", 1L));
                                    }
                                });


                            }
                        }
                ).build();
            }
        }
        return patternDatabase;
    }

    private static List<Tag> generateTags() {
        ArrayList<Tag> tagArrayList = new ArrayList<>();

        tagArrayList.add(new Tag("hello"));
        tagArrayList.add(new Tag("world"));
        tagArrayList.add(new Tag("awesome"));
        return tagArrayList;
    }


    @SuppressWarnings("SpellCheckingInspection")
    private static List<FearlessPattern> generatePatternData() {
        List<FearlessPattern> patternList = new ArrayList<>();

        patternList.add(new FearlessPattern("Accentuate the Positive", "To influence others during the change initiative and inspire them to believe the change can happen, motivate them with a sense of hope rather than fear.", "Your attempts to scare others are not working.", "Inspire people throughout the change initiative with a sense of optimism rather than fear."));
        patternList.add(new FearlessPattern("Concrete Action Plan", "To make progress toward your goal, state precisely what you will do as you take the next baby step.", "Leading a change initiative, with its many twists and turns and ever-growing list of things to do, can make you feel out of control.", "Describe the next small step for reaching a milestone goal in terms of concrete actions that include what you will do, where, and when."));
        patternList.add(new FearlessPattern("Easier Path", "To encourage adoption of a new idea, experiment with removing obstacles that might be standing in the way.", "What can you do to make it easier for people to change?, \"Change the environment in a way that will encourage people to adopt the new idea.", ""));
        patternList.add(new FearlessPattern("Elevator Pitch", "Have a couple of sentences on hand to introduce others to your new idea.", "When you have a chance to introduce someone to your idea, you don’t want to stumble around for the right words to say.", "Craft a couple of sentences that contain your key message."));
        patternList.add(new FearlessPattern("Emotional Connection", "Connecting with the feelings of your audience is usually more effective in persuading them than just presenting facts.", "As you share information about your new idea, you might believe that logical argument is enough to persuade people.", "Create a connection with individuals on an emotional level by listening and addressing how they are feeling about the new idea."));
        patternList.add(new FearlessPattern("Evolving Vision", "While taking baby steps through a change process, periodically set aside time for reflection to reevaluate your vision.", "A lofty vision can seem attainable in the beginning, but can become unrealistic when the world changes during the process.", "Use an iterative approach to learn about and refine your vision."));
        patternList.add(new FearlessPattern("Future Commitment", "To make it more likely that you will get help in the change initiative, ask others to do something you will need much later and wait for them to commit.", "You need help, but people are busy.", "Approach individuals with an item that isn’t urgent so they can put it on their to-do list on a future date."));
        patternList.add(new FearlessPattern("Go-To Person", "Identify key people who can help with critical issues in your change initiative.", "Once you’ve identified areas where you lack expertise, how do you start asking for help?, \"Make a concrete action plan with a list of the things you need to do for the next milestone. Next to each item, write the names of those individuals with the specific expertise or resources to help you accomplish the task.", ""));
        patternList.add(new FearlessPattern("Imagine That", "To kick-start the change initiative, engage others in an exercise to imagine future possibilities.", "It can be difficult for those you are trying to convince to see how a new idea will fit into the work they will be doing.", "Ask people to imagine a possible outcome with the new idea.  Begin with “What if...?”\n\"Know Yourself"));
        patternList.add(new FearlessPattern("Myth Buster", "Identify misconceptions surrounding the change initiative and address them in a timely and forthright manner.", "If we hear someone express an incorrect assumption about the innovation, we usually address it head-on with the person who is expressing the concern. However, a false impression in one person’s mind is usually a sign that this viewpoint is shared by others.", "To get the word out about what the innovation isn’t as well as what won’t happen as a result of its introduction into the organization, create a simple list of the myths paired with the realities."));
        patternList.add(new FearlessPattern("Pick Your Battles", "Before you expend your energy in conflict, ask yourself whether you believe the issue is really important and if you have the resources to carry your fight through to the end.", "You can’t spend time and energy addressing every bit of resistance you meet.", "Stop. Take a deep breath and think for a minute. Ask yourself if the current conflict is worth it. Overcome your initial emotional reaction and make a conscious decision to fight only for those things that will make a difference. Maintain your integrity so that at the end of each decision point you are proud of yourself."));
        patternList.add(new FearlessPattern("Town Hall Meeting", "As early as possible and throughout the initiative, schedule an event to share updates about the new idea, solicit feedback, build support, uncover new ideas, and bring in newcomers.", "It is difficult to stay in touch and involve everyone during the long period of time that is often necessary for a change initiative.", "Hold a meeting to solicit feedback, build support, get new ideas, intrigue newcomers, and report progress."));
        patternList.add(new FearlessPattern("Wake-up Call", "To encourage people to pay attention to your idea, point out the issue that you believe has created a pressing need for change.", "People in your organization seem to be comfortable with the status quo. They don’t see the need to change the current state of things., \"Create a conscious need for the change by calling attention to a problem and its negative consequences in the organization.", ""));
        patternList.add(new FearlessPattern("Ask for Help", "Since the task of introducing a new idea into an organization is a big job, look for people and resources to help your efforts and encourage involvement.", "The job of introducing a new idea into an organization is too big for one person, especially a newcomer who doesn't know the ropes.", "Ask as many people as you can for help when you need it.  Don’t try to do it alone."));
        patternList.add(new FearlessPattern("Baby Steps", "(previously titled Step-By-Step) Take one small step at a time toward your goal.", "You wonder what your plan should be for introducing the new idea into your organization.", "Use an incremental approach in the change initiative, with short- term goals, while keeping your long-term vision."));
        patternList.add(new FearlessPattern("Big Jolt", "To provide visibility for the change effort, hold a high- profile event to showcase the new idea.", "You’ve been carrying out some activities to give your new idea some visibility in your organization, but at some point you need to attract more attention to the effort.", "Arrange for a high-profile person who can talk about the new idea to do a presentation in your organization."));
        patternList.add(new FearlessPattern("Bridge Builder", "Ask those who have accepted the new idea to talk with those who have not.", "Some won’t listen to even the most enthusiastic proponent if it’s someone they don’t know or trust.", "Ask for help from early adopters, connectors, or gurus who have already adopted the innovation. Introduce them to people who have interests similar to theirs and encourage them to discuss how they found the innovation useful."));
        patternList.add(new FearlessPattern("Brown Bag", "Use the time when people normally eat together as a convenient and relaxed setting for hearing about the new idea.", "People can be too busy to attend optional meetings held during work hours.", "Hold the meeting in the middle of the day and invite attendees to bring their own lunches."));
        patternList.add(new FearlessPattern("ChampionSkeptic", "Ask for help from opinion leaders who are skeptical of your new idea, and use their comments to improve your effort, even if you don’t change their minds.", "Some of the resisters to the new idea are strong opinion leaders in your organization.", "Ask for help from a skeptical opinion leader to play the role of “official skeptic” or “official realist.”\n\"Connector"));
        patternList.add(new FearlessPattern("Corporate Angel", "To help align the innovation with the goals of the organization, get support from a high-level executive.", "Support from local management will provide some attention and resources for the new idea, but you need high-level support to have a more lasting impact.", "Enlist the support of a high-level executive who has a special interest in the new idea and will provide direction and the resources to support it."));
        patternList.add(new FearlessPattern("Corridor Politics", "Informally work on decision makers and key influence's before an important vote, to ensure they understand the consequences of the decision.", "It’s difficult to address the concerns of all decision makers when a new idea is raised in a large meeting.", "Informally work on decision makers and key influencers one-on- one before the vote. Try to get the approval of anyone who can kill the idea."));
        patternList.add(new FearlessPattern("Dedicated Champion", "To increase your effectiveness in introducing your new idea, make a case for having the work become part of your job description.", "Effectively introducing a new idea into any organization is too much work for a volunteer.", "Make a case for including the change initiative as part of your job description."));
        patternList.add(new FearlessPattern("Do Food", "To influence attendees, bring special food to a meeting.", "Usually a meeting is just another ordinary, impersonal event.", "Make food available at the meeting."));
        patternList.add(new FearlessPattern("Early Adopter", "Win the support of the people who can be opinion leaders for the new idea.", "To create more impact for the new idea in an organization, interest must extend beyond the initial group of supporters.", "Look for the opinion leaders and ask them for help."));
        patternList.add(new FearlessPattern("Early Majority", "To increase support, show that many people are starting to use the innovation.", "The support of innovators and early adopters will spark the new idea, but you need much more to truly have impact.", "Expand the group that has adopted the new idea rapidly to include the more deliberate majority that will allow the new idea to establish a strong foothold."));
        patternList.add(new FearlessPattern("Evangelist", "To begin to introduce the new idea into your organization, do everything you can to share your passion for it.", "You want to get a new idea going, but you don’t know where to start.", "To introduce a new idea, let your passion for this new idea drive you."));
        patternList.add(new FearlessPattern("External Validation", "To increase the credibility of the new idea, bring in information from sources outside the organization.", "Before being persuaded to accept a new idea, people want assurance that the idea has validity outside the organization.", "Give people in the organization external sources of useful information about the new idea."));
        patternList.add(new FearlessPattern("Fear Less", "Turn resistance to the new idea to your advantage by respectfully listening to and learning from skeptics’ point of view.", "Any innovation is disruptive, so resistance is likely.", "Ask for help from resisters."));
        patternList.add(new FearlessPattern("Group Identity", "Give the change effort an identity but encourage wide participation to involve everyone.", "It’s harder to introduce a new idea when people aren’t aware that the effort exists.", "Give the change effort an identity."));
        patternList.add(new FearlessPattern("Guru on Your Side", "Enlist the support of influential people who are esteemed by members of the organization at all levels.", "People in an organization can be reluctant to show interest in a new idea unless it has the support of colleagues they respect.", "Enlist the support of experienced, senior-level gurus who are respected by both managers and non-managers alike."));
        patternList.add(new FearlessPattern("Guru Review", "Gather a group of trusted advisors and other interested colleagues to evaluate the new idea for managers and other developers.", "Some managers and developers are supportive, but others are reluctant to join in until they have some assurance that this is a worthwhile idea.", "Gather a review team of respected gurus in the organization to evaluate the new idea."));
        patternList.add(new FearlessPattern("Hometown Story", "To help people see the usefulness of the new idea, encourage those who have had success with it to share their stories in an informal setting.", "People who haven’t used the new idea may not be aware that other people have used it successfully.", "Encourage individuals to share their experiences with the new idea in an informal, highly interactive session."));
        patternList.add(new FearlessPattern("Innovator", "When you begin the change initiative, ask for help from colleagues who like new ideas.", "You need people to jumpstart the new idea in your organization.", "Find the people who are quick to adopt new ideas. Talk to them about the innovation and ask for help in sparking an interest for it in the organization."));
        patternList.add(new FearlessPattern("Involve Everyone", "For a new idea to be successful across an organization, everyone should have an opportunity to make his or her own unique contribution.", "Even when you ask for help, there’s a tendency to take on too much. Others---especially those who don’t see the value in the new idea---may think of it as “your show.”, \"Make it known that everyone is welcome to be part of the change effort. Involve people from as many different groups as possible: management, administrative and technical support, marketing, and training.", ""));
        patternList.add(new FearlessPattern("Just Do It", "Don’t wait for the perfect moment when you have the resources and knowledge you think you need; instead, take the first baby step and start learning.", "You don’t have any experience with the innovation yourself, just good ideas that might work. You believe that the innovation can help the organization, but you’re not sure.", "Gather firsthand information on the benefits and limitations of the innovation by integrating it into your current work."));
        patternList.add(new FearlessPattern("Just Enough", "To ease people into the new idea, avoid over-selling and overwhelming them by providing an appropriate amount of information that they can understand and use at that particular time.", "Difficult, complex concepts can overwhelm novices.", "When introducing the new idea, concentrate on the fundamentals and give learners a brief description of the more difficult concepts. Provide more information when they are ready."));
        patternList.add(new FearlessPattern("Local Sponsor", "Ask for help from first-line management; when your boss supports the tasks you are doing to introduce the new idea, you can be more effective.", "You need attention and resources for the new idea.", "Find a first-line manager to support your new idea---ideally, your boss."));
        patternList.add(new FearlessPattern("Location", " Location", " Location\"", "When holding an event that focuses on the new idea, consider the comfort and enjoyment of the participants so the surroundings do not interfere with their ability to listen and participate."));
        patternList.add(new FearlessPattern("Mentor", "When a project team wants to get started with the new idea, have someone around who understands it and can help the team.", "People want to use the new idea on their project but don’t know how to begin.", "Find an outside or internal consultant or trainer to provide mentoring and feedback while project members are getting started with the innovation."));
        patternList.add(new FearlessPattern("Next Steps", "Take time near the end of an event or conversation to identify which actions participants can do next.", "A presentation in a training class or another event can leave attendees uncertain about what to do with what they have learned.", "Take time near the end of a presentation to brainstorm and discuss how the participants can apply the new information."));
        patternList.add(new FearlessPattern("Persistent PR", "(previously titled In Your Space) To keep the new idea in front of everyone, consistently promote it in a variety of ways.", "Unless people are reminded, they may forget about the new idea.", "Post information about the new idea around your organization--- wherever people are likely to see it and discuss it."));
        patternList.add(new FearlessPattern("Personal Touch", "To convince people of the value in a new idea, show how it can be personally useful and valuable to them.", "Presentations and training will arouse curiosity and some interest in the new idea, but you must do more---the old habits of most individuals will not die without effort.", "Talk with individuals about the ways in which the new idea can be personally useful and valuable to them."));
        patternList.add(new FearlessPattern("Piggyback", "To help the new idea be less threatening, build on existing practices and use current language.", "Several procedures or hurdles are required for the introduction of your new idea but you’re looking for an easier way.", "Piggyback the new idea on a well-accepted practice in the organization."));
        patternList.add(new FearlessPattern("Plant the Seeds", "Take every opportunity you can, no matter how small, to spark an interest in the idea.", "You want to spark some interest in the new idea.", "Carry materials about the new idea to events where people gather. Put them in places where people are likely to pick them up and look at them."));
        patternList.add(new FearlessPattern("The Right Time", "Consider the timing of competing obligations when you schedule events or when you ask for help.", "When people face deadlines and have too much to do, they tend to focus on things that move them toward completing necessary tasks and making the deadlines.", "Be aware of those times when people are likely to be the busiest.  Schedule events and requests for help outside those times."));
        patternList.add(new FearlessPattern("Royal Audience", "Arrange for management and members of the organization to spend time with a special visitor.", "You want to get the most out of a visit from a famous person.", "Use spare hours or lunchtime during the day or evenings, before and/or after the featured presentation, to make the visitor available for teams, individuals, or managers."));
        patternList.add(new FearlessPattern("Shoulder to Cry On", "To avoid becoming too discouraged when the going gets tough, find opportunities for everyone to have supportive listeners.", "When you’re struggling to introduce a new idea, it’s easy to become discouraged.", "Get together regularly with others who are also working to introduce the new idea or are interested in the process."));
        patternList.add(new FearlessPattern("Sincere Appreciation", "(previously titled Just Say Thanks) To help people feel appreciated, express your gratitude in the most sincere way you can to everyone who makes a contribution.", "People feel unappreciated when they work hard and no one notices or cares.", "Find everyone who has helped you and say “thanks” in the most sincere way you can."));
        patternList.add(new FearlessPattern("Small Successes", "To avoid becoming discouraged by obstacles and slow progress, celebrate even a small success.", "Every organizational change effort has its ups and downs. It’s a difficult process.", "As you carry on in baby steps, take the time to recognize and celebrate successes, especially the small ones."));
        patternList.add(new FearlessPattern("Smell of Success", "When your efforts produce a visibly positive result, treat this opportunity as a teaching moment.", "When you start to have some success, newcomers will ask you about the innovation.", "When people comment on the success they see with the innovation, treat their inquiry as a teaching moment."));
        patternList.add(new FearlessPattern("Stay in Touch", "Once you’ve sparked some interest in people, don’t forget about them, and make sure they don’t forget about you.", "Your key supporters have too many things to think about and can forget about the new idea.", "Stay in touch with your key supporters."));
        patternList.add(new FearlessPattern("Study Group", "Form a small group of colleagues who are interested in exploring or continuing to learn about your new idea.", "There may be little or no money for formal training on the specific topic.", "Form a group of no more than eight colleagues who are interested in exploring and studying an interesting topic."));
        patternList.add(new FearlessPattern("Sustained Momentum", "Be proactive in keeping your change initiative going.", "The many other things that need to be done will tempt you to put the task of introducing the new idea on the back burner for a while. Doing so can cause you and other people to lose interest in it.", "Take a proactive approach in the organization to the ongoing work of sustaining the interest in the new idea. Take some small action each day, no matter how insignificant it may seem, to move you closer to your goal."));
        patternList.add(new FearlessPattern("Tailor Made", "To convince management and executives in the organization, point out the costs and benefits of your new idea.", "Individuals can be intrigued by interesting ideas, but to have impact on an organization, the idea has to be more than just interesting.", "Taylor your message about the innovation to the needs of the organization."));
        patternList.add(new FearlessPattern("Time for Reflection", "To learn from the past, take time at regular intervals to evaluate what is working well and what should be done differently.", "We make the same assumptions and the same mistakes based on those assumptions over and over again.", "Pause in any activity to reflect on what is working well and what should be done differently."));
        patternList.add(new FearlessPattern("Token", "To keep a new idea alive in a person’s memory, give tokens, especially valuable intangibles that can be identified with the topic being introduced.", "People may be enthusiastic about a topic when they first hear about it, but the enthusiasm quickly wanes as they forget tomorrow what excited them today.", "Hand out small tokens that will remind people of the new idea."));
        patternList.add(new FearlessPattern("Trial Run", "When the organization is reluctant to commit to the new idea, suggest an experiment for a short period and learn from its results.", "There are people in the organization who are expressing an endless stream of objections to the new idea. It would be a daunting, or even impossible, task to try to ease everyone’s worries before the new idea is adopted.", "Suggest that the organization, or a segment of the organization, try the new idea for a limited period as an experiment."));
        patternList.add(new FearlessPattern("Whisper in the General’s Ear", "Because managers and others at any level of authority are usually hard to convince in a group setting, meet privately to address any concerns.", "Managers who are against your new idea have the power to block your progress.", "Set up a short one-on-one meeting with a manager to address any concerns with the innovation and the effort to introduce it."));

        for (int i = 0; i < 3; i++) {
            patternList.get(i).setFavorite(true);
        }
        return patternList;
    }

    private static List<Scenario> generateDummyScenarios() {
        ArrayList<Scenario> scenarios = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            scenarios.add(new Scenario("Ich habe noch kein Mittag gegessen" + i, "Ich habe hunger und würde Gern was zu mittag essen" + i, "Kein Essen Im Kühlschrank" + i));
        }
        return scenarios;

    }


}
