package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.UserDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.UserLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.RemoteUserDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

public class UserViewModel extends AndroidViewModel {


    private RemoteUserDataSource userRemoteDataSource;
    private UserLocalDataSource userLocalDataSource;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userLocalDataSource = Injection.provideLocalUserRepository(application.getApplicationContext());
        userRemoteDataSource = Injection.provideRemoteUserRepository();
    }

    public LiveData<User> registerUser(RemoteUser newUser) {
        final MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();
        userRemoteDataSource.registerUser(newUser, new UserDataSource.GetUserCallback() {
            @Override
            public void onUserLoaded(User user) {
                userLocalDataSource.saveCreatedUser(user);
                userMutableLiveData.setValue(user);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return userMutableLiveData;

    }

    public LiveData<User> queryRegisteredUser() {
        final MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();
        userLocalDataSource.queryUser(new UserDataSource.GetUserCallback() {
            @Override
            public void onUserLoaded(User user) {
                userMutableLiveData.setValue(user);
            }

            @Override
            public void onDataNotAvailable() {
                userMutableLiveData.setValue(null);
            }
        });
        return userMutableLiveData;

    }

}
