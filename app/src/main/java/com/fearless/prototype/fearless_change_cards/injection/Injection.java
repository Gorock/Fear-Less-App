package com.fearless.prototype.fearless_change_cards.injection;

import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.PatternNoteDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.PatternNoteRepository;
import com.fearless.prototype.fearless_change_cards.data.source.PatternRepository;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioRepository;
import com.fearless.prototype.fearless_change_cards.data.source.StrategyDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.StrategyRepository;
import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.TagRepository;
import com.fearless.prototype.fearless_change_cards.data.source.local.FavoriteLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.PatternDatabase;
import com.fearless.prototype.fearless_change_cards.data.source.local.PatternLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.PatternNoteLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.TagLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.local.UserLocalDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.PatternRestClient;
import com.fearless.prototype.fearless_change_cards.data.source.remote.RemotePatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.RemoteScenarioDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.RemoteStrategyDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.RemoteUserDataSource;

import java.util.concurrent.TimeUnit;

import io.openapitools.jackson.dataformat.hal.HALMapper;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

// Provides DataSources to the ViewModel
// Datasources can be Switched to change the Behavior of the App
// It Also Provides a Rest client called Retrofit
public class Injection {

    static Injection instance;
    private User user;

    private Injection() {
    }

    public static synchronized Injection getInstance() {
        if (instance == null)
            return new Injection();
        else
            return instance;
    }


    public void setCurrentUser(User currentUser) {
        this.user = currentUser;
    }

    public User getCurrentUser() {
        return user;
    }

    private static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static PatternDataSource providePatternRepository(@NonNull Context context) {
        PatternDatabase database = PatternDatabase.getINSTANCE(context);
        return PatternRepository.getINSTANCE(new RemotePatternDataSource());
    }

    public static FavoriteLocalDataSource provideFavoriteLocalDataSource(@NonNull Context context) {
        PatternDatabase database = PatternDatabase.getINSTANCE(context);
        return FavoriteLocalDataSource.getInstance(database.fearlessFavoriteDao());
    }

    public static PatternNoteDataSource providePatternNoteRepository(@NonNull Context context) {
        PatternDatabase database = PatternDatabase.getINSTANCE(context);
        return PatternNoteRepository.getINSTANCE(
                PatternNoteLocalDataSource.getInstance(database.fearLessPatternNoteDao()));
    }

    public static ScenarioDataSource provideScenarioRepository(@NonNull Context context) {
        return ScenarioRepository.getINSTANCE(new RemoteScenarioDataSource());
    }

    public static TagDataSource provideTagRepository(@NonNull Context context) {
        PatternDatabase database = PatternDatabase.getINSTANCE(context);
        return TagRepository.getINSTANCE(
                TagLocalDataSource.getInstance(database.fearLessTagDao(),
                        database.fearLessPatternTagJoinDao()));
    }

    public static StrategyDataSource provideStrategyRepository(@NonNull Context context) {
        return StrategyRepository.getINSTANCE(
                new RemoteStrategyDataSource()
        );
    }

    public static Retrofit provideRetrofit() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();


        return new Retrofit.Builder()
                .baseUrl("http://ADVVS25.gm.fh-koeln.de")
                //These are Only For Testing locally
//                .baseUrl("http://192.168.2.121:8081")
//                .baseUrl("http://192.168.178.32")
//                .baseUrl("http://192.168.178.32:8080")
                .addConverterFactory(JacksonConverterFactory.create(new HALMapper()))
                .client(okHttpClient)
                .build();
    }


    public static PatternRestClient providePatternRestClient() {
        Retrofit r = provideRetrofit();
        return r.create(PatternRestClient.class);
    }


    public static UserLocalDataSource provideLocalUserRepository(@NonNull Context context) {
        PatternDatabase database = PatternDatabase.getINSTANCE(context);
        return UserLocalDataSource.getInstance(database.fearLessUserDao());

    }

    public static RemoteUserDataSource provideRemoteUserRepository() {
        return new RemoteUserDataSource();
    }

}
