package com.fearless.prototype.fearless_change_cards.activities.scenario;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.Keyboard;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.RegisterPopup;
import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.ScenarioViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

import java.util.List;
import java.util.Objects;

public class ScenariosActivity extends AppCompatActivity {

    private ScenarioViewModel scenariosViewModel;
    private UserViewModel userViewModel;

    private PopupWindow popupWindow;
    private ScenarioCardFactory scenarioCardFactory;

    private User registeredUser;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scenarios);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        scenariosViewModel = ViewModelProviders.of(this).get(ScenarioViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        ViewGroup scenarioList = findViewById(R.id.scenario_list);
        FavoriteViewModel favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        this.scenarioCardFactory = new ScenarioCardFactory(scenarioList,
                getLayoutInflater(),
                scenariosViewModel,
                userViewModel, this, favoriteViewModel);

        FloatingActionButton createScenarioButton = findViewById(R.id.fab);

        userViewModel.queryRegisteredUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null && user.getSelfLink() != null && !user.getSelfLink().isEmpty()) {
                    popupWindow = createAddScenariosPopupWindow();
                    ScenariosActivity.this.registeredUser = user;
                } else {
                    LayoutInflater inflater = (LayoutInflater)
                            ScenariosActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final RegisterPopup registerPopup = new RegisterPopup(
                            Objects.requireNonNull(inflater),
                            userViewModel,
                            ScenariosActivity.this,
                            new Observer<User>() {
                                @Override
                                public void onChanged(@Nullable User user) {
                                    popupWindow = createAddScenariosPopupWindow();
                                    ScenariosActivity.this.registeredUser = user;
                                }
                            });

                    popupWindow = registerPopup.getPopupWindow();
                }
            }
        });

        createScenarioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                //show the Keyboard
                Keyboard.showKeyboard(ScenariosActivity.this);
            }
        });

        BottomNavigationView selectedView = findViewById(R.id.bottomNavigationView);
        selectedView.setSelectedItemId(R.id.scenario_button);

        scenariosViewModel.getScenarios().observe(this,
                new Observer<List<Scenario>>() {
                    @Override
                    public void onChanged(@Nullable List<Scenario> scenarios) {
                        showScenarios(scenarios);
                    }
                });
    }


    private PopupWindow createAddScenariosPopupWindow() {

        LayoutInflater inflater = (LayoutInflater) ScenariosActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_insert_scenario, null);

        final PopupWindow addScenarioWindow = new PopupWindow(layout,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true);


        final Button addScenarioBtn = addScenarioWindow.getContentView().findViewById(R.id.add_scenario_btn);
        addScenarioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addScenarioWindow.dismiss();
                EditText scenarioTitleText = v.getRootView().findViewById(R.id.scenario_title_text);
                EditText scenarioDescriptionText = v.getRootView().findViewById(R.id.scenario_description_text);
                EditText scenarioProblemText = v.getRootView().findViewById(R.id.scenario_problem_text);
                scenariosViewModel.addScenario(scenarioTitleText.getText().toString(),
                        scenarioDescriptionText.getText().toString(),
                        scenarioProblemText.getText().toString(),
                        registeredUser.getSelfLink()).observe(ScenariosActivity.this,
                        new Observer<Scenario>() {
                            @Override
                            public void onChanged(@Nullable Scenario scenario) {
                                scenarioCardFactory.addScenarioCard(scenario);
                            }
                        });
            }
        });
        return addScenarioWindow;
    }


    public void showScenarios(List<Scenario> scenarios) {
        ViewGroup scenarioList = findViewById(R.id.scenario_list);
        scenarioList.removeAllViews();
        for (Scenario scenario : scenarios) {
            scenarioCardFactory.addScenarioCard(scenario);
        }

    }
}
