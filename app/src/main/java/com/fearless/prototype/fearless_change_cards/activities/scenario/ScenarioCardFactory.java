package com.fearless.prototype.fearless_change_cards.activities.scenario;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.ViewSetter;
import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.ScenarioViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

public class ScenarioCardFactory {

    private final ViewGroup scenarioList;
    private final LayoutInflater layoutInflater;
    private final ScenarioViewModel scenarioViewModel;
    private final AppCompatActivity owner;
    private final FavoriteViewModel favoriteViewModel;
    private final UserViewModel userViewModel;

    public ScenarioCardFactory(
            ViewGroup scenarioList,
            LayoutInflater layoutInflater,
            ScenarioViewModel scenarioViewModel,
            UserViewModel userViewModel, AppCompatActivity owner, FavoriteViewModel favoriteViewModel) {

        this.scenarioList = scenarioList;
        this.userViewModel = userViewModel;
        this.layoutInflater = layoutInflater;
        this.scenarioViewModel = scenarioViewModel;
        this.owner = owner;
        this.favoriteViewModel = favoriteViewModel;
    }

    public CardView addScenarioCard(final Scenario scenario) {

        final CardView card = (CardView) layoutInflater.inflate(R.layout.template_scenario, scenarioList, false);
        final ViewSetter viewSetter = new ViewSetter(card);

        String titleString = scenario.getName();
        // the static check is here wrong the Name of the Scenario can be null
        if (titleString != null) {
            titleString = scenario.getName();
        }
        viewSetter.setText(R.id.template_scenario_title, titleString);
        viewSetter.setText(R.id.scenarioId, String.valueOf(scenario.getPrimaryId()));
        viewSetter.setText(R.id.scenarioDescription, scenario.getDescription());
        viewSetter.setText(R.id.thumbsUp, " " + scenario.getThumbsUp());

        userViewModel.queryRegisteredUser().observe(owner, new Observer<User>() {
            @Override
            public void onChanged(@Nullable final User currentUser) {
                scenarioViewModel.getCreatorOfScenario(scenario).observe(owner, new Observer<User>() {
                    @Override
                    public void onChanged(@Nullable User creator) {
                        if (currentUser != null && creator != null && currentUser.getSelfLink().equals(creator.getSelfLink())) {
                            ImageButton button = card.findViewById(R.id.delete_scenario_button);
                            button.setVisibility(View.VISIBLE);
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    scenarioViewModel.deleteScenario(scenario);
                                    ((ViewGroup) card.getParent()).removeView(card);
                                }

                            });
                        }
                    }
                });
            }
        });

        addThumbsUpOnClickListener(scenario, card);
        addFavoriteOnClickListener(scenario, card);
        addOnStrategyCardListener(scenario, card);

        if (scenario.getStrategies() != null)
            viewSetter.setText(R.id.scenariosNumberOfStrategies, String.valueOf(scenario.getStrategies().size()));

        scenarioList.addView(card);
        return card;
    }

    private void addOnStrategyCardListener(final Scenario scenario, CardView card) {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(owner, SingleScenarioDetail.class);
                myIntent.putExtra("scenarioId", scenario.getPrimaryId());
                myIntent.putExtra("scenarioUri", scenario.getSelfLink());
                myIntent.setFlags(myIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                owner.startActivity(myIntent, Bundle.EMPTY);

            }
        });
    }

    private void addFavoriteOnClickListener(final Scenario scenario, CardView card) {
        final CheckBox favoriteSwitch = card.findViewById(R.id.scenariosLike);
        if (favoriteSwitch != null) {
            favoriteViewModel.queryFavorite(scenario.getSelfLink()).observe(owner, new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean aBoolean) {
                    favoriteSwitch.setChecked(aBoolean);
                }
            });
            favoriteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean favoriteStatus) {
                    if (favoriteStatus)
                        ScenarioCardFactory.this
                                .favoriteViewModel.addFavorite(scenario.getSelfLink());
                    else
                        ScenarioCardFactory.this
                                .favoriteViewModel.removeFavorite(scenario.getSelfLink());


                }
            });
        }
    }

    private void addThumbsUpOnClickListener(final Scenario scenario, CardView card) {
        final CheckBox thumbsUp = card.findViewById(R.id.thumbsUp);
        thumbsUp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    scenarioViewModel.addValueToThumbsUp(scenario, 1)
                            .observe(owner, new Observer<Scenario>() {
                                @Override
                                public void onChanged(@Nullable Scenario scenario) {
                                    if (scenario == null)
                                        return;
                                    scenario.setThumbsUp(scenario.getThumbsUp());
                                    String thumbsUpString = " " + scenario.getThumbsUp();
                                    thumbsUp.setText(thumbsUpString);
                                }
                            });
                else
                    scenarioViewModel.addValueToThumbsUp(scenario, -1)
                            .observe(owner, new Observer<Scenario>() {
                                @Override
                                public void onChanged(@Nullable Scenario scenario) {
                                    if (scenario == null)
                                        return;
                                    scenario.setThumbsUp(scenario.getThumbsUp());
                                    String thumbsUpString = " " + scenario.getThumbsUp();
                                    thumbsUp.setText(thumbsUpString);
                                }
                            });
            }
        });
    }

}
