package com.fearless.prototype.fearless_change_cards.activities.scenario;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.strategies.CreateStrategyActivity;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.GetCurrentUserObserver;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.StrategyCardViewFactory;
import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.ScenarioViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.StrategiesViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.UserViewModel;

import java.util.Objects;

public class SingleScenarioDetail extends AppCompatActivity {


    private ScenarioViewModel scenarioViewModel;
    private final String tag = getClass().getSimpleName();
    private StrategyCardViewFactory strategyCardViewFactory;
    private StrategiesViewModel strategyViewModel;
    private FavoriteViewModel favoriteViewModel;
    private ViewGroup strategyViewGroup;
    private UserViewModel userViewModel;
    private ScenarioCardFactory scenarioCardFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scenarios_detail);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        Intent intent = getIntent();
        final Long scenarioId = (Long) Objects.requireNonNull(intent.getExtras()).get("scenarioId");
        final String scenarioUri = (String) Objects.requireNonNull(intent.getExtras()).get("scenarioUri");

        ViewGroup scenarioField = findViewById(R.id.scenarioPlace);
        this.strategyViewModel = ViewModelProviders.of(this).get(StrategiesViewModel.class);
        this.favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        this.userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        this.scenarioViewModel = ViewModelProviders.of(this).get(ScenarioViewModel.class);
        this.scenarioCardFactory = new ScenarioCardFactory(scenarioField, getLayoutInflater(), scenarioViewModel, userViewModel, this, favoriteViewModel);
        this.strategyViewGroup = findViewById(R.id.list_strategy_lin);

        strategyCardViewFactory = new StrategyCardViewFactory(this, this, strategyViewModel, favoriteViewModel, userViewModel, strategyViewGroup);
        scenarioViewModel.getScenarioWithUriThenId(scenarioUri, scenarioId).observe(this,
                new Observer<Scenario>() {
                    @Override
                    public void onChanged(@Nullable Scenario scenario) {
                        showScenario(scenario);
                    }
                });


        BottomNavigationView selectedView = findViewById(R.id.bottomNavigationView);
        selectedView.setSelectedItemId(R.id.scenario_button);


        final FloatingActionButton addScenarioButton = findViewById(R.id.add_strategy_fab);
        addScenarioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userViewModel.queryRegisteredUser().observe(SingleScenarioDetail.this,
                        new GetCurrentUserObserver(SingleScenarioDetail.this,
                                userViewModel,
                                getLayoutInflater(),
                                addScenarioButton, new GetCurrentUserObserver.UserFound() {
                            @Override
                            public void onUserFound(User user) {
                                Intent myIntent = new Intent(SingleScenarioDetail.this, CreateStrategyActivity.class);
                                myIntent.putExtra("scenarioId", scenarioId);
                                myIntent.putExtra("scenarioUri", scenarioUri);
                                startActivity(myIntent);
                            }
                        }));
            }
        });

    }


    public void showScenario(final Scenario scenario) {
        Log.d(SingleScenarioDetail.class.getSimpleName(), scenario.toString());
        this.scenarioCardFactory.addScenarioCard(scenario);

        for (Strategy strategy : scenario.getStrategies()) {
            strategyCardViewFactory.createStrategyCard(strategy);
        }
        Space space = new Space(this);
        space.setMinimumHeight(256);
        strategyViewGroup.addView(space);


        Log.d(tag, scenario.toString());
    }
}
