package com.fearless.prototype.fearless_change_cards.activities.library;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.BottomBar;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.PatternCardViewFactory;
import com.fearless.prototype.fearless_change_cards.data.model.Favorite;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.PatternViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.SearchPatternsViewModel;

import java.util.List;

public class ListViewActivity extends AppCompatActivity implements PatternShower {

    private PatternCardViewFactory cardFactory;
    private ViewGroup parent;
    private FavoriteViewModel favoriteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libary_list);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        parent = findViewById(R.id.cardList);

        PatternViewModel patternViewModel = ViewModelProviders.of(this).get(PatternViewModel.class);
        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        patternViewModel.getPatterns().observe(this, new Observer<List<FearlessPattern>>() {
            @Override
            public void onChanged(@Nullable List<FearlessPattern> patterns) {
                showPatterns(patterns);
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.pattern_button);


        BottomNavigationView topView = findViewById(R.id.top_view_selection);
        LibaryTopNavigation.createTopNavigation(topView, this, R.id.ListButton);


        PatternSearcher.makePatternSearcher(this,
                (SearchView) findViewById(R.id.searchView),
                ViewModelProviders.of(this).get(SearchPatternsViewModel.class),
                this);


        BottomNavigationItemView gridButton = findViewById(R.id.GridButton);
        gridButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToGridView();
            }
        });

        cardFactory = new PatternCardViewFactory(this, this, R.layout.template_pattern_card_list,
                parent, patternViewModel, favoriteViewModel);
    }

    private void switchToGridView() {
        Intent myIntent = new Intent(ListViewActivity.this, GridViewActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(myIntent);
    }


    public void showPatterns(List<FearlessPattern> patterns) {
        if (patterns != null) {
            parent.removeAllViews();
            for (FearlessPattern pattern : patterns) {
                parent.addView(cardFactory.makeCard(pattern));
            }
        }
    }
}
