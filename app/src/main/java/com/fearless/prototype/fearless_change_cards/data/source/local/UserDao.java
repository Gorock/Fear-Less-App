package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.fearless.prototype.fearless_change_cards.data.model.User;

@Dao
public interface UserDao {


    @Query("Select * from User limit 1")
    User getUser();

    @Insert
    void saveCreatedUser(User user);
}
