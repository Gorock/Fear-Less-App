package com.fearless.prototype.fearless_change_cards.data.source.remote.mappers;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.Challenge;

import java.util.ArrayList;
import java.util.List;

import io.openapitools.jackson.dataformat.hal.HALLink;

public class ChallengeScenarioMapper {

    private ChallengeScenarioMapper() {

    }


    public static Challenge map(Scenario scenario) {
        HALLink.Builder builder = new HALLink.Builder(scenario.getSelfLink());

        Challenge challenge = new Challenge(scenario.getName(),
                scenario.getDescription(),
                scenario.getThumbsUp(),
                scenario.getResolved(),
                builder.build()
        );
        challenge.setTitle(scenario.getName());
        HALLink.Builder link = new HALLink.Builder(scenario.getSelfLink());
        challenge.setSelf(link.build());

        challenge.setDescription(scenario.getDescription());
        challenge.setResolved(scenario.getResolved());
        challenge.setThumbsUp(scenario.getThumbsUp());
        return challenge;
    }


    public static Scenario map(Challenge remoteScenario) {

        Scenario result = new Scenario(
                remoteScenario.getTitle(),
                remoteScenario.getDescription(),
                "",
                "",
                remoteScenario.getThumbsUp(),
                remoteScenario.isResolved()
        );

        result.setSelfLink(remoteScenario.getSelf().getHref());
        result.setCreatorLink(remoteScenario.getCreator().getHref());
        return result;
    }

    public static List<Scenario> map(List<Challenge> remoteScenarios) {
        List<Scenario> fearlessPatternList = new ArrayList<>();

        for (Challenge remote : remoteScenarios) {
            fearlessPatternList.add(ChallengeScenarioMapper.map(remote));
        }
        return fearlessPatternList;
    }

}
