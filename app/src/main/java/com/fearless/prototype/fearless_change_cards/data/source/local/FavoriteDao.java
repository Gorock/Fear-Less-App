package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fearless.prototype.fearless_change_cards.data.model.Favorite;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

@Dao
public interface FavoriteDao {


    @Query("SELECT * from FAVORITE where resourceUri =:resouceUri")
    Favorite queryIsFavorite(String resouceUri);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addToFavorite(Favorite newFavorite);

    @Delete
    void removeFromFavorite(Favorite oldFavorite);

}
