package com.fearless.prototype.fearless_change_cards.activities.library;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;

import com.fearless.prototype.fearless_change_cards.R;

public class LibaryTopNavigation {

    private LibaryTopNavigation() {
    }

    public static void createTopNavigation(BottomNavigationView topNavigation,
                                           final Context packageContext,
                                           int selectedItemId) {


        BottomNavigationItemView libraryButton = topNavigation.findViewById(R.id.ListButton);
        libraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToListView(packageContext);
            }
        });


        BottomNavigationItemView gridButton = topNavigation.findViewById(R.id.GridButton);
        gridButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToGridView(packageContext);
            }
        });
        topNavigation.setSelectedItemId(selectedItemId);

        disableDetailButton(topNavigation);
    }

    private static void switchToListView(Context packageContext) {
        Intent myIntent = new Intent(packageContext, ListViewActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        packageContext.startActivity(myIntent);
    }


    private static void disableDetailButton(BottomNavigationView topNavigation) {
        topNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return item.getItemId() != R.id.DetailButton;
            }
        });
    }


    private static void switchToGridView(Context packageContext) {
        Intent myIntent = new Intent(packageContext, GridViewActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        packageContext.startActivity(myIntent);
    }

}
