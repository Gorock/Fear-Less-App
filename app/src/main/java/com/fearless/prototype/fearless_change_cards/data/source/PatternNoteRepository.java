package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;

public class PatternNoteRepository implements PatternNoteDataSource {


    private static PatternNoteRepository instance = null;

    private final PatternNoteDataSource patternNoteSqlLiteSource;


    private PatternNoteRepository(@NonNull PatternNoteDataSource patternNoteSqlLiteSource) {
        this.patternNoteSqlLiteSource = patternNoteSqlLiteSource;

    }

    public static PatternNoteRepository getINSTANCE(PatternNoteDataSource dataSource) {

        if (instance == null) {
            instance = new PatternNoteRepository(dataSource);
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    @Override
    public void removeNote(PatternNote patternNote, GetNotesCallback callback) {
        patternNoteSqlLiteSource.removeNote(patternNote, callback);
    }

    @Override
    public void getNoteWithPatternId(@NonNull long patternId, @NonNull GetNotesCallback callback) {
        patternNoteSqlLiteSource.getNoteWithPatternId(patternId, callback);
    }

    @Override
    public void saveNote(@NonNull PatternNote patternNote, @NonNull GetNotesCallback callback) {
        patternNoteSqlLiteSource.saveNote(patternNote, callback);
    }

    @Override
    public void refreshNote() {
        throw new UnsupportedOperationException();
    }

}

