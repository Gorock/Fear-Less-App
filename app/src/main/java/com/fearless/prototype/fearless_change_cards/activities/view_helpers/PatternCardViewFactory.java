package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.listeners.OnCardClickListener;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.PatternViewModel;

public class PatternCardViewFactory {
    private final FavoriteViewModel favoriteViewModel;
    private LayoutInflater layoutInflater;
    private ViewGroup root;
    private PatternViewModel patternViewModel;
    private Context context;
    private LifecycleOwner owner;
    private int resource;

    public PatternCardViewFactory(Context context, LifecycleOwner owner, int cardResource, ViewGroup root,
                                  PatternViewModel patternViewModel,
                                  FavoriteViewModel favoriteViewModel) {
        this.resource = cardResource;
        this.context = context;
        this.owner = owner;
        this.root = root;
        this.patternViewModel = patternViewModel;
        this.favoriteViewModel = favoriteViewModel;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
    }

    public CardView makeCard(final FearlessPattern pattern) {
        final CardView card = this.getCardView();
        final CheckBox favoriteSwitch = card.findViewById(R.id.favoriteSwitch);
        favoriteViewModel.queryFavorite(pattern.getSelfLink()).observe(owner, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    pattern.setFavorite(aBoolean);
                    favoriteSwitch.setChecked(aBoolean);
                }
            }
        });
        PatternCardViewFactory.this.addFavoriteSwitchListener(pattern, card);
        this.setTextOnCardView(pattern, card);
        card.setOnClickListener(new OnCardClickListener(context, pattern));
        return card;

    }

    private CardView getCardView() {
        assert layoutInflater != null;
        return (CardView) layoutInflater.inflate(resource, root, false);
    }

    private void setTextOnCardView(FearlessPattern pattern, CardView card) {
        ViewSetter viewSetter = new ViewSetter(card);
        viewSetter.setText(R.id.pattern_title, pattern.getName());
        viewSetter.setText(R.id.patternId, String.valueOf(pattern.getId()));
        viewSetter.setText(R.id.description, pattern.getDescription());
    }

    private void addFavoriteSwitchListener(final FearlessPattern pattern, CardView card) {
        CheckBox favoriteSwitch = card.findViewById(R.id.favoriteSwitch);
        favoriteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean favoriteStatus) {
                if (favoriteStatus)
                    favoriteViewModel.addFavorite(pattern.getSelfLink());
                else
                    favoriteViewModel.removeFavorite(pattern.getSelfLink());
            }
        });
    }


}