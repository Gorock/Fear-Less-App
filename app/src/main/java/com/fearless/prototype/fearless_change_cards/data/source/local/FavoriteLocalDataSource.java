package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Favorite;

public class FavoriteLocalDataSource {


    public interface FavoriteCallback {
        void onFavoriteQuerySuccess(boolean favorite);
    }

    private static volatile FavoriteLocalDataSource instance;
    private static volatile FavoriteDao favoriteDao;

    private FavoriteLocalDataSource() {
    }

    public static FavoriteLocalDataSource getInstance(@NonNull FavoriteDao favoriteDao) {
        if (instance == null) {
            synchronized (FavoriteLocalDataSource.class) {
                if (instance == null) {
                    instance = new FavoriteLocalDataSource();
                    FavoriteLocalDataSource.favoriteDao = favoriteDao;
                }
            }
        }
        return instance;
    }


    public void queryFavorite(String resourceUri, FavoriteLocalDataSource.FavoriteCallback callback) {
        QueryFavoriteTask task = new QueryFavoriteTask(favoriteDao, resourceUri, callback);
        task.execute();
    }


    private static class QueryFavoriteTask extends AsyncTask<Void, Void, Favorite> {

        private final String resourceUri;
        FavoriteDao favoriteDao;
        FavoriteCallback callback;

        private QueryFavoriteTask(FavoriteDao favoriteDao, String resourceUri, FavoriteCallback callback) {
            this.favoriteDao = favoriteDao;
            this.resourceUri = resourceUri;
            this.callback = callback;
        }

        @Override
        protected Favorite doInBackground(Void... voids) {
            return this.favoriteDao.queryIsFavorite(resourceUri);

        }

        @Override
        protected void onPostExecute(Favorite favorite) {
            if (favorite != null)
                callback.onFavoriteQuerySuccess(true);
            else
                callback.onFavoriteQuerySuccess(false);
        }
    }


    public void addFavorite(Favorite favorite) {
        AddFavoriteTask task = new AddFavoriteTask(favoriteDao, favorite);
        task.execute();
    }


    private static class AddFavoriteTask extends AsyncTask<Void, Void, Void> {

        private final Favorite favorite;
        FavoriteDao favoriteDao;

        private AddFavoriteTask(FavoriteDao favoriteDao, Favorite favorite) {
            this.favoriteDao = favoriteDao;
            this.favorite = favorite;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            this.favoriteDao.addToFavorite(favorite);
            return null;
        }
    }


    public void removeFavorite(Favorite favorite) {
        RemoveFavoriteTask task = new RemoveFavoriteTask(favoriteDao, favorite);
        task.execute();

    }

    private static class RemoveFavoriteTask extends AsyncTask<Void, Void, Void> {

        private final Favorite favorite;
        FavoriteDao favoriteDao;

        private RemoveFavoriteTask(FavoriteDao favoriteDao, Favorite favorite) {
            this.favoriteDao = favoriteDao;
            this.favorite = favorite;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            this.favoriteDao.removeFromFavorite(favorite);
            return null;
        }
    }
}
