package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

public class FearlessPatternWithTags {
    @Embedded
    public FearlessPattern pattern;

    @Relation(parentColumn = "patternId", entityColumn = "fpatternId")
    public List<Tag> tagsList;


    @Override
    public String toString() {
        return "FearlessPatternWithTags{" +
                "pattern=" + pattern +
                ", tagsList=" + tagsList +
                '}';
    }
}
