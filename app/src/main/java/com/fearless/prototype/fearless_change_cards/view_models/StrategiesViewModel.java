package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.StrategyDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class StrategiesViewModel extends AndroidViewModel {

    private StrategyDataSource strategyDataSource;
    private MutableLiveData<List<Strategy>> liveStrategyData;


    public StrategiesViewModel(@NonNull Application application) {
        super(application);
        strategyDataSource = Injection.provideStrategyRepository(application.getApplicationContext());
    }

    public LiveData<List<Strategy>> getStrategies() {
        if (liveStrategyData == null)
            liveStrategyData = new MutableLiveData<>();
        strategyDataSource.getStrategies(new StrategyDataSource.GetStrategiesCallback() {
            @Override
            public void onStrategiesLoaded(List<Strategy> strategies) {
                liveStrategyData.setValue(strategies);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException("Not Implemented Yet");
            }
        });
        return liveStrategyData;


    }


    public LiveData<Strategy> saveStrategy(Strategy strategy) {
        final MutableLiveData<Strategy> savedStrategy = new MutableLiveData<>();
        strategyDataSource.saveStrategy(strategy, new StrategyDataSource.GetStrategyCallback() {
            @Override
            public void onStrategyLoaded(Strategy strategy) {
                savedStrategy.setValue(strategy);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException("not implemented Yet");
            }
        });
        return savedStrategy;
    }

    public LiveData<Boolean> deleteStrategy(Strategy strategy) {
        final MutableLiveData<Boolean> success = new MutableLiveData<>();
        strategyDataSource.deleteStrategy(strategy, new StrategyDataSource.OnStragegiesDeletetionCallback() {
            @Override
            public void onSuccess() {
                success.setValue(true);
            }

            @Override
            public void onFailure() {
                success.setValue(false);

            }
        });
        return success;
    }
}
