package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class SearchPatternsViewModel extends AndroidViewModel {


    private PatternDataSource patternDataSource;
    private MutableLiveData<List<FearlessPattern>> livePatternList;

    public SearchPatternsViewModel(@NonNull Application application) {
        super(application);
        this.patternDataSource = Injection.providePatternRepository(application.getApplicationContext());
    }

    public LiveData<List<FearlessPattern>> searchPatterns(String searchTerm) {
        if (livePatternList == null)
            livePatternList = new MutableLiveData<>();
        patternDataSource.searchPattern(searchTerm, new PatternDataSource.GetPatternsCallback() {
            @Override
            public void onPatternsLoaded(List<FearlessPattern> patterns) {
                livePatternList.setValue(patterns);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return livePatternList;
    }

}
