package com.fearless.prototype.fearless_change_cards.data.source.remote;

import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.ChallangeList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.PatternList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.StrategyList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.Challenge;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteStrategy;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface PatternRestClient {
    @GET("/patterns?size=100")
    Call<PatternList> getAllPatterns();

    @GET("/patterns?size=1")
    Call<PatternList> getPatternAtIndex(@Query("page") Long index);

    @GET("/challenges")
    Call<ChallangeList> getAllChallanges();

    @PUT
    Call<Challenge> putChallange(@Url String uri, @Body Challenge challenge);


    @GET("/challenges?size=1")
    Call<ChallangeList> getChallangeAtIndex(@Query("page") Long index);

    @GET("/strategies?size=1")
    Call<StrategyList> getStrategyAtIndex(@Query("page") Long index);

    @GET("/strategies")
    Call<StrategyList> getStrategies();


    @DELETE
    Call<Object> deleteViaUrl(@Url String Uri);


    @Headers("Accept: application/json")
    @POST("/strategies")
    Call<RemoteStrategy> createStrategy(@Body RemoteStrategy strategy);

    @GET
    Call<StrategyList> getStrategiesWithUrl(@Url String uri);

    @GET
    Call<PatternList> getPatternsWithUrl(@Url String uri);


    @GET
    Call<RemoteUser> getUserWithUrl(@Url String uri);

    @GET
    Call<Challenge> getChallangeWithUrl(@Url String uri);


    @Headers("Accept: application/json")
    @POST("/users")
    Call<RemoteUser> createUser(@Body RemoteUser user);


    @Headers("Accept: application/json")
    @POST("/challenges")
    Call<Challenge> postChallenge(@Body Challenge challenge);


    @Headers({"Accept: application/json",
            "Content-Type: text/uri-list"})
    @PUT
    Call<Void> postLink(@Url String uri, @Body RequestBody uriResourceToLink);
}




