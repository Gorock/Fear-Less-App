package com.fearless.prototype.fearless_change_cards.data.source.remote;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Strategy;
import com.fearless.prototype.fearless_change_cards.data.source.StrategyDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.StrategyList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteStrategy;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteUser;
import com.fearless.prototype.fearless_change_cards.data.source.remote.mappers.StrategyMapper;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class RemoteStrategyDataSource implements StrategyDataSource {

    private final PatternRestClient patternRestClient;

    public RemoteStrategyDataSource() {
        patternRestClient = Injection.providePatternRestClient();
    }


    @Override
    public void deleteStrategy(Strategy strategy, OnStragegiesDeletetionCallback deletetionCallback) {
        DeleteStrategyTask task = new DeleteStrategyTask(patternRestClient, strategy, deletetionCallback);
        task.execute();
    }


    private static class DeleteStrategyTask extends AsyncTask<Void, Void, Boolean> {
        private final PatternRestClient patternRestClient;
        private final Strategy strategy;
        private final OnStragegiesDeletetionCallback deletetionCallback;

        private DeleteStrategyTask(PatternRestClient patternRestClient, Strategy strategy, OnStragegiesDeletetionCallback deletetionCallback) {
            this.patternRestClient = patternRestClient;
            this.strategy = strategy;
            this.deletetionCallback = deletetionCallback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Response<Object> response = patternRestClient.deleteViaUrl(strategy.getSelfLink()).execute();
                return response.isSuccessful();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean wasSuccessful) {
            if (wasSuccessful)
                deletetionCallback.onSuccess();
            else
                deletetionCallback.onFailure();
        }
    }


    @Override
    public void getStrategies(@NonNull GetStrategiesCallback callback) {
        GetStrategiesTask task = new GetStrategiesTask(patternRestClient, callback);
        task.execute();
    }

    private static class GetStrategiesTask extends AsyncTask<Void, Void, List<Strategy>> {
        private final PatternRestClient patternRestClient;
        private final GetStrategiesCallback callback;

        public GetStrategiesTask(PatternRestClient patternRestClient, GetStrategiesCallback callback) {
            this.patternRestClient = patternRestClient;
            this.callback = callback;
        }


        @Override
        protected List<Strategy> doInBackground(Void... voids) {
            try {
                Response<StrategyList> strategyListRespone = patternRestClient.getStrategies().execute();
                if (strategyListRespone.isSuccessful() && strategyListRespone.body() != null) {
                    List<RemoteStrategy> strategyLists = strategyListRespone.body().getStrategies();
                    ArrayList<Strategy> strategyArrayList = new ArrayList<>();
                    for (RemoteStrategy remoteStrategy : strategyLists) {
                        Response<RemoteUser> remoteUserResponse = patternRestClient.getUserWithUrl(remoteStrategy.getCreator().getHref()).execute();
                        Strategy strategy = StrategyMapper.map(remoteStrategy);
                        if (remoteUserResponse.isSuccessful()) {
                            strategy.setUsername(remoteUserResponse.body().getUsername());
                        }
                        strategyArrayList.add(strategy);
                    }
                    return strategyArrayList;
                }
            } catch (
                    IOException e)

            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Strategy> strategies) {
            if (strategies != null) {
                callback.onStrategiesLoaded(strategies);
            } else {
                callback.onDataNotAvailable();
            }
        }
    }


    @Override
    public void getStrategy(@NonNull String scenarioId, @NonNull GetStrategyCallback callback) {
        GetStrategyTask task = new GetStrategyTask(scenarioId, patternRestClient, callback);
        task.execute();

    }

    private static class GetStrategyTask extends AsyncTask<Void, Void, Strategy> {
        private final GetStrategyCallback callback;
        private final PatternRestClient patternRestClient;
        private final String scenarioId;

        public GetStrategyTask(String scenarioId, PatternRestClient patternRestClient, GetStrategyCallback callback) {
            this.scenarioId = scenarioId;
            this.patternRestClient = patternRestClient;
            this.callback = callback;
        }


        @Override
        protected Strategy doInBackground(Void... voids) {
            Long id = Long.parseLong(scenarioId);
            Response<StrategyList> strategyListResponse;
            try {
                strategyListResponse = patternRestClient.getStrategyAtIndex(id).execute();
                if (strategyListResponse.isSuccessful()) {
                    List<RemoteStrategy> strategyList = strategyListResponse.body().getStrategies();
                    if (!strategyList.isEmpty()) {
                        return StrategyMapper.map(strategyList).get(0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Strategy strategy) {
            if (strategy != null) {
                callback.onStrategyLoaded(strategy);
            } else {
                callback.onDataNotAvailable();
            }
        }

    }


    @Override
    public void saveStrategy(@NonNull Strategy strategy, @NonNull GetStrategyCallback callback) {
        SaveStrategyTask saveStrategyTask = new SaveStrategyTask(strategy, patternRestClient, callback);
        saveStrategyTask.execute();
    }

    private static class SaveStrategyTask extends AsyncTask<Void, Void, Strategy> {
        private Strategy strategy;
        private PatternRestClient patternRestClient;
        private GetStrategyCallback callback;

        SaveStrategyTask(Strategy strategy, PatternRestClient patternRestClient,
                         GetStrategyCallback callback) {
            this.strategy = strategy;
            this.patternRestClient = patternRestClient;
            this.callback = callback;
        }

        @Override
        protected Strategy doInBackground(Void... voids) {
            try {
                Response<RemoteStrategy> remoteStrategyResponse = patternRestClient.createStrategy(StrategyMapper.map(strategy)).execute();
                if (remoteStrategyResponse.isSuccessful()) {
                    RemoteStrategy remoteStrategy = remoteStrategyResponse.body();

                    RequestBody challangeUriBody = RequestBody.create(MediaType.parse("text/plain"), strategy.getChallangeLink());
                    Response<Void> linkToChallengeResponse = patternRestClient.postLink(remoteStrategy.getChallenge().getHref(), challangeUriBody).execute();

                    RequestBody creatorUriBody = RequestBody.create(MediaType.parse("text/plain"), strategy.getCreatorLink());
                    Response<Void> linkToCreator = patternRestClient.postLink(remoteStrategy.getCreator().getHref(), creatorUriBody).execute();

                    StringBuilder linkStringBuilder = new StringBuilder();
                    for (FearlessPattern pattern : strategy.getFearlessPattern()) {
                        linkStringBuilder.append(pattern.getSelfLink());
                        linkStringBuilder.append("\n");
                    }
                    RequestBody patternLinksBody = RequestBody.create(MediaType.parse("text/plain"), linkStringBuilder.toString());
                    Response<Void> linkToPatterns = patternRestClient.postLink(remoteStrategy.getPatterns().getHref(), patternLinksBody).execute();

                    if (linkToChallengeResponse.isSuccessful() && linkToCreator.isSuccessful() && linkToPatterns.isSuccessful()) {
                        Strategy createdStrategy = StrategyMapper.map(remoteStrategy);
                        return createdStrategy;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Strategy strategy) {
            if (strategy == null)
                callback.onDataNotAvailable();
            else
                callback.onStrategyLoaded(strategy);
        }
    }
}
