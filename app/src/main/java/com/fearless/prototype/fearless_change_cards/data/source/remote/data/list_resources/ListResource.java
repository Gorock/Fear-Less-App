package com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListResource {

    @Link
    private HALLink first;
    @Link
    private HALLink next;
    @Link
    private HALLink prev;
    @Link
    private HALLink last;

    @JsonProperty("page")
    private Page page;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }


    public HALLink getFirst() {
        return first;
    }

    public void setFirst(HALLink first) {
        this.first = first;
    }

    public HALLink getNext() {
        return next;
    }

    public void setNext(HALLink next) {
        this.next = next;
    }

    public HALLink getLast() {
        return last;
    }

    public void setLast(HALLink last) {
        this.last = last;
    }

    public HALLink getPrev() {
        return prev;
    }

    public void setPrev(HALLink prev) {
        this.prev = prev;
    }
}
