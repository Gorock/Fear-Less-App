package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "Tag")
public final class Tag {

    @PrimaryKey
    @NonNull
    private String name;


    public Tag(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tag{" +
                ", name='" + name + '\'' +
                '}';
    }
}
