package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.view.View;
import android.widget.TextView;

public class ViewSetter {
    private View root;

    public ViewSetter(View root) {
        this.root = root;
    }

    public void setText(int id, String text) {
        TextView creatorText = root.findViewById(id);
        if (creatorText != null)
            creatorText.setText(text);
    }
}


