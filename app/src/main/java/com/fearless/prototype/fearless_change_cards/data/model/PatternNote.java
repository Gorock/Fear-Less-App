package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity
public class PatternNote {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "noteId")
    private Long primaryId;

    private Long patternId;
    private String text;


    public PatternNote(String text, Long patternId) {
        this.text = text;
        this.patternId = patternId;
    }

    @Override
    public String toString() {
        return "FearLessPatternNote{" +
                "primaryId=" + primaryId +
                ", text='" + text + '\'' +
                '}';
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPatternId(Long patternId) {
        this.patternId = patternId;
    }

    public Long getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(Long primaryId) {
        this.primaryId = primaryId;
    }

    public Long getPatternId() {
        return patternId;
    }
}
