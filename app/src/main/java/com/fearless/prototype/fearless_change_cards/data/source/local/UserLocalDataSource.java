package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.UserDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

public class UserLocalDataSource {


    private static volatile UserLocalDataSource instance;
    private static volatile UserDao userDao;


    public static UserLocalDataSource getInstance(@NonNull UserDao userDao) {
        if (instance == null) {
            synchronized (TagLocalDataSource.class) {
                if (instance == null) {
                    instance = new UserLocalDataSource();
                    UserLocalDataSource.userDao = userDao;
                }
            }
        }
        return instance;
    }


    public void saveCreatedUser(User user) {
        new SaveCreatedUserTask(user).execute();

    }


    private static class SaveCreatedUserTask extends AsyncTask<Void, Void, Void> {

        private final User user;

        SaveCreatedUserTask(User user) {
            this.user = user;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userDao.saveCreatedUser(this.user);
            return null;
        }
    }

    public void queryUser(UserDataSource.GetUserCallback callback) {
        new QueryUserTask(callback).execute();
    }

    private class QueryUserTask extends AsyncTask<Void, Void, User> {


        private final UserDataSource.GetUserCallback callback;

        QueryUserTask(UserDataSource.GetUserCallback callback) {
            this.callback = callback;
        }


        @Override
        protected User doInBackground(Void... voids) {
            return userDao.getUser();
        }

        @Override
        protected void onPostExecute(User user) {
            if (user == null)
                callback.onDataNotAvailable();
            else {
                callback.onUserLoaded(user);
            }
        }
    }
}
