package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

@Entity(tableName = "Scenario")
public final class Scenario {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "scenarioId")
    private Long primaryId;


    private String selfLink;

    @Nullable
    @ColumnInfo(name = "solution")
    private final String solution;

    @NonNull
    @ColumnInfo(name = "problem")
    private final String problem;

    @NonNull
    @ColumnInfo(name = "name")

    @SerializedName("title")
    private String name;

    @NonNull
    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String description;


    @NonNull
    @ColumnInfo(name = "numberOfLikes")
    private Integer thumbsUp;


    @NonNull
    @ColumnInfo(name = "isResolved")
    private Boolean isResolved;

    @NonNull
    @ColumnInfo(name = "Favorite")
    private Boolean isFavorite;

    @Ignore
    private List<Strategy> strategies;

    private String creatorLink;

    private String username;


    public Scenario(@NonNull String name,
                    @NonNull String description,
                    @NonNull String problem,
                    @Nullable String solution,
                    @NonNull Integer thumbsUp,
                    @NonNull Boolean isResolved
    ) {
        this.thumbsUp = thumbsUp;
        this.isResolved = isResolved;
        this.name = name;
        this.description = description;
        this.problem = problem;
        this.solution = solution;
        this.isFavorite = false;
    }

    @Ignore
    public Scenario(
            @NonNull String name,
            @NonNull String description,
            @NonNull String problem) {
        this(name, description, problem, "", 0, false);
    }


    @NonNull
    public Long getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(Long primaryId) {
        this.primaryId = primaryId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "Scenario{" +
                "primaryId='" + primaryId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scenario scenario = (Scenario) o;
        return Objects.equals(primaryId, scenario.primaryId) &&
                Objects.equals(name, scenario.name) &&
                Objects.equals(description, scenario.description);

    }

    @Override
    public int hashCode() {
        return Objects.hash(primaryId, name, description);
    }

    @NonNull
    public Integer getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(@NonNull Integer thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    @Nullable
    public String getSolution() {
        return solution;
    }

    @NonNull
    public String getProblem() {
        return problem;
    }

    @NonNull
    public Boolean getResolved() {
        return isResolved;
    }

    public void setResolved(@NonNull Boolean resolved) {
        isResolved = resolved;
    }

    @NonNull
    public Boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(@NonNull Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }


    public String getCreatorLink() {
        return creatorLink;
    }

    public void setCreatorLink(String creatorLink) {
        this.creatorLink = creatorLink;
    }

    public List<Strategy> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<Strategy> strategies) {
        this.strategies = strategies;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

