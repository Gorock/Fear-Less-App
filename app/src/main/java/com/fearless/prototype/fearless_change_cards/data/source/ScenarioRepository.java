package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;

import java.util.List;

public class ScenarioRepository implements ScenarioDataSource {

    private static ScenarioRepository instance = null;

    private final ScenarioDataSource localScenarioDataSource;
    private final ScenarioDataSource firstDataSource;


    private ScenarioRepository(@NonNull ScenarioDataSource scenarioDataSource, ScenarioDataSource firstDataSource) {
        this.localScenarioDataSource = scenarioDataSource;
        this.firstDataSource = firstDataSource;
    }

    public static ScenarioRepository getINSTANCE(ScenarioDataSource dataSource,
                                                 ScenarioDataSource firstDataSource) {

        if (instance == null) {
            instance = new ScenarioRepository(dataSource, firstDataSource);
        }
        return instance;
    }

    public static ScenarioRepository getINSTANCE(ScenarioDataSource dataSource) {
        return getINSTANCE(dataSource, dataSource);
    }

    public static void destroyInstance() {
        instance = null;
    }


    @Override
    public void getScenarios(@NonNull final GetScenariosCallback callback) {
        this.firstDataSource.getScenarios(new GetScenariosCallback() {
            @Override
            public void onScenariosLoaded(List<Scenario> scenarios) {
                callback.onScenariosLoaded(scenarios);
            }

            @Override
            public void onDataNotAvailable() {
                localScenarioDataSource.getScenarios(callback);

            }
        });
    }

    @Override
    public void getScenario(@NonNull final String scenarioId, @NonNull final GetScenarioCallback callback) {
        this.firstDataSource.getScenario(scenarioId, new GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                callback.onScenarioLoaded(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                localScenarioDataSource.getScenario(scenarioId, callback);
            }
        });
    }

    @Override
    public void getScenarioWithUri(@NonNull final String uri, @NonNull final GetScenarioCallback callback) {
        this.firstDataSource.getScenarioWithUri(uri, new GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                callback.onScenarioLoaded(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                localScenarioDataSource.getScenarioWithUri(uri, callback);
            }
        });
    }

    @Override
    public void saveScenario(@NonNull Scenario scenario, @NonNull GetScenarioCallback callback) {
        localScenarioDataSource.saveScenario(scenario, callback);
    }

    @Override
    public void insertScenario(@NonNull final Scenario scenario, @NonNull final GetScenarioCallback callback) {
        firstDataSource.insertScenario(scenario, new GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                callback.onScenarioLoaded(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                localScenarioDataSource.insertScenario(scenario, callback);
            }
        });
    }

    @Override
    public void getScenarioCreator(@NonNull Scenario scenario, @NonNull ScenarioCreatorCallback callback) {
        firstDataSource.getScenarioCreator(scenario, callback);
    }

    @Override
    public void deleteScenario(@NonNull Scenario scenario, @NonNull DeleteScenarioCallback callback) {
        firstDataSource.deleteScenario(scenario, callback);
    }
}

