package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Tag;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource.GetTagsCallback;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class TagsViewModel extends AndroidViewModel {

    private TagDataSource tagDataSource;

    private MutableLiveData<List<Tag>> tagsLiveData;

    public TagsViewModel(@NonNull Application application) {
        super(application);
        tagDataSource = Injection.provideTagRepository(application.getApplicationContext());
    }


    public LiveData<List<Tag>> getTagsForPatternId(long id) {
        if (tagsLiveData == null)
            tagsLiveData = new MutableLiveData<>();
        tagDataSource.getTags(id, new TagsCallback());
        return tagsLiveData;

    }

    public LiveData<List<Tag>> addTagToPatternWithId(long patternId, String tagName) {
        if (tagsLiveData == null)
            tagsLiveData = new MutableLiveData<>();
        tagDataSource.saveTagWithPattern(new Tag(tagName), patternId, new TagsCallback());
        return tagsLiveData;
    }

    public LiveData<List<Tag>> removeTagFromPatternWithId(long patternId, Tag tag) {
        if (tagsLiveData == null)
            tagsLiveData = new MutableLiveData<>();
        tagDataSource.removeTagWithPattern(tag, patternId, new TagsCallback());
        return tagsLiveData;
    }


    private class TagsCallback implements GetTagsCallback {

        @Override
        public void onTagsLoaded(List<Tag> tags) {
            tagsLiveData.setValue(tags);
        }

        @Override
        public void onDataNotAvailable() {
            throw new UnsupportedOperationException();
        }
    }

}
