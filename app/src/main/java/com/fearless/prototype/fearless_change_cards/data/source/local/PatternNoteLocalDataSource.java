package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;
import com.fearless.prototype.fearless_change_cards.data.source.PatternNoteDataSource;

import java.util.List;

public class PatternNoteLocalDataSource implements PatternNoteDataSource {


    private static volatile PatternNoteLocalDataSource instance;

    private static volatile PatternNoteDao patternNoteDao;


    public static PatternNoteLocalDataSource getInstance(@NonNull PatternNoteDao patternNoteDao) {
        if (instance == null) {
            synchronized (ScenarioLocalDataSource.class) {
                if (instance == null) {
                    instance = new PatternNoteLocalDataSource();
                    PatternNoteLocalDataSource.patternNoteDao = patternNoteDao;
                }
            }
        }
        return instance;
    }


    @Override
    public void getNoteWithPatternId(long patternId, @NonNull GetNotesCallback callback) {
        FindPatternNoteForPatternWithIdTask task = new FindPatternNoteForPatternWithIdTask(patternId, callback);
        task.execute();
    }

    private static class FindPatternNoteForPatternWithIdTask extends AsyncTask<Void, Void, List<PatternNote>> {

        PatternNoteDataSource.GetNotesCallback callback;
        Long patternId;

        FindPatternNoteForPatternWithIdTask(Long patternId, PatternNoteDataSource.GetNotesCallback callback) {
            this.callback = callback;
            this.patternId = patternId;
        }

        @Override
        protected List<PatternNote> doInBackground(Void... voids) {
            return patternNoteDao.findPatternNoteForPatternWithId(patternId);
        }

        @Override
        protected void onPostExecute(List<PatternNote> patternNotes) {
            this.callback.onNotesLoaded(patternNotes);
        }
    }


    @Override
    public void saveNote(@NonNull PatternNote patternNote, GetNotesCallback callback) {
        new SaveNoteTask(patternNote, callback).execute();
    }

    private static class SaveNoteTask extends AsyncTask<Void, Void, List<PatternNote>> {

        PatternNote note;
        GetNotesCallback callback;

        SaveNoteTask(PatternNote note, GetNotesCallback callback) {
            this.note = note;
            this.callback = callback;
        }

        @Override
        protected List<PatternNote> doInBackground(Void... voids) {
            patternNoteDao.addPersonalNote(note);
            return patternNoteDao.findPatternNoteForPatternWithId(note.getPatternId());
        }

        @Override
        protected void onPostExecute(List<PatternNote> notes) {
            callback.onNotesLoaded(notes);
        }
    }

    @Override
    public void refreshNote() {
        throw new UnsupportedOperationException("Not implemented");
    }


    @Override
    public void removeNote(PatternNote patternNote, GetNotesCallback callback) {
        RemoveNoteFromPatternWithIdTask task = new RemoveNoteFromPatternWithIdTask(patternNote, callback);
        task.execute();
    }


    private static class RemoveNoteFromPatternWithIdTask extends AsyncTask<Void, Void, List<PatternNote>> {
        PatternNote patternNote;
        GetNotesCallback callback;

        private RemoveNoteFromPatternWithIdTask(PatternNote patternNote, GetNotesCallback callback) {
            this.patternNote = patternNote;
            this.callback = callback;
        }

        @Override
        protected List<PatternNote> doInBackground(Void... voids) {
            patternNoteDao.removePatternNote(patternNote);
            return patternNoteDao.findPatternNoteForPatternWithId(patternNote.getPatternId());
        }

        @Override
        protected void onPostExecute(List<PatternNote> patternNotes) {
            if (patternNotes != null)
                callback.onNotesLoaded(patternNotes);
            else
                callback.onDataNotAvailable();
        }
    }
}

