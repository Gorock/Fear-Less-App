package com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
public class RemoteUser {

    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;

    @Link("self")
    private HALLink self;
    @Link("user")
    private HALLink user;
    @Link("createdChallenges")
    private HALLink createdChallenges;
    @Link("createdStrategies")
    private HALLink createdStrategies;
    @Link("givenComments")
    private HALLink givenComments;


    public RemoteUser() {
    }


    public RemoteUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }

    public HALLink getUser() {
        return user;
    }

    public void setUser(HALLink user) {
        this.user = user;
    }

    public HALLink getCreatedChallenges() {
        return createdChallenges;
    }

    public void setCreatedChallenges(HALLink createdChallenges) {
        this.createdChallenges = createdChallenges;
    }

    public HALLink getCreatedStrategies() {
        return createdStrategies;
    }

    public void setCreatedStrategies(HALLink createdStrategies) {
        this.createdStrategies = createdStrategies;
    }

    public HALLink getGivenComments() {
        return givenComments;
    }

    public void setGivenComments(HALLink givenComments) {
        this.givenComments = givenComments;
    }
}
