package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

@Entity(tableName = "fearless_tag_join",
        primaryKeys = {"patternId", "tagName"})
public class FearLessPatternTagJoin {
    @NonNull
    public final String tagName;
    @NonNull
    public final Long patternId;

    public FearLessPatternTagJoin(long patternId, @NonNull String tagName) {
        this.tagName = tagName;
        this.patternId = patternId;
    }
}
