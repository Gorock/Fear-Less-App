package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioDataSource;

import java.util.List;


public class ScenarioLocalDataSource implements ScenarioDataSource {

    private static volatile ScenarioLocalDataSource instance;

    private static volatile ScenarioDao scenarioDao;


    public static ScenarioLocalDataSource getInstance(@NonNull ScenarioDao scenarioDao) {
        if (instance == null) {
            synchronized (ScenarioLocalDataSource.class) {
                if (instance == null) {
                    instance = new ScenarioLocalDataSource();
                    ScenarioLocalDataSource.scenarioDao = scenarioDao;
                }
            }
        }
        return instance;
    }


    @Override
    public void getScenarios(@NonNull GetScenariosCallback callback) {
        GetScenariosTask task = new GetScenariosTask(callback);
        task.execute();
    }

    private static class GetScenariosTask extends AsyncTask<Void, Void, List<Scenario>> {

        private GetScenariosCallback callback;

        GetScenariosTask(GetScenariosCallback callback) {
            this.callback = callback;
        }


        @Override
        protected List<Scenario> doInBackground(Void... voids) {
            return scenarioDao.getScenarios();
        }

        @Override
        protected void onPostExecute(List<Scenario> scenarios) {
            this.callback.onScenariosLoaded(scenarios);
        }

    }


    @Override
    public void getScenario(@NonNull String scenarioId, @NonNull GetScenarioCallback callback) {
        GetSingleScenarioTask task = new GetSingleScenarioTask(callback);
        task.execute(scenarioId);
    }


    private static class GetSingleScenarioTask extends AsyncTask<String, Void, Scenario> {

        private GetScenarioCallback callback;

        GetSingleScenarioTask(GetScenarioCallback callback) {
            this.callback = callback;
        }


        @Override
        protected Scenario doInBackground(String... strings) {
            return scenarioDao.getScenario(strings[0]);
        }

        protected void onPostExecute(Scenario scenario) {
            this.callback.onScenarioLoaded(scenario);
        }

    }


    @Override
    public void saveScenario(@NonNull Scenario scenario, @NonNull GetScenarioCallback callback) {
        SaveScenarioTask task = new SaveScenarioTask(callback);
        task.execute(scenario);
    }


    public static class SaveScenarioTask extends AsyncTask<Scenario, Void, Scenario> {
        private GetScenarioCallback callback;

        SaveScenarioTask(GetScenarioCallback callback) {
            this.callback = callback;
        }

        @Override
        protected Scenario doInBackground(Scenario... scenarios) {
            scenarioDao.updateScenario(scenarios[0]);
            return scenarios[0];
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            this.callback.onScenarioLoaded(scenario);
        }
    }

    @Override
    public void insertScenario(@NonNull Scenario scenario, @NonNull GetScenarioCallback callback) {
        InsertScenarioTask task = new InsertScenarioTask(scenario, callback);
        task.execute();
    }


    private static class InsertScenarioTask extends AsyncTask<Void, Void, Scenario> {

        private final Scenario scenario;
        private final GetScenarioCallback callback;

        private InsertScenarioTask(Scenario scenario, GetScenarioCallback callback) {
            this.scenario = scenario;
            this.callback = callback;
        }

        @Override
        protected Scenario doInBackground(Void... voids) {
            try {
                long scenarioId = scenarioDao.insert(scenario);
                scenario.setPrimaryId(scenarioId);
                return scenario;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null)
                callback.onScenarioLoaded(scenario);
            else
                callback.onDataNotAvailable();

        }
    }


    @Override
    public void getScenarioCreator(@NonNull Scenario scenario, @NonNull ScenarioCreatorCallback callback) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void deleteScenario(@NonNull Scenario scenario, @NonNull DeleteScenarioCallback callback) {
        throw new UnsupportedOperationException("implent if needed");

    }


    @Override
    public void getScenarioWithUri(@NonNull String uri, @NonNull GetScenarioCallback callback) {
        GetScenarioWithUriTask task = new GetScenarioWithUriTask(uri, callback);
        task.execute();
    }

    private static class GetScenarioWithUriTask extends AsyncTask<Void, Void, Scenario> {

        String uri;
        GetScenarioCallback callback;

        public GetScenarioWithUriTask(String uri, GetScenarioCallback callback) {
            this.uri = uri;
            this.callback = callback;
        }

        @Override
        protected Scenario doInBackground(Void... voids) {
            return scenarioDao.getScenarioWithUri(uri);
        }

        @Override
        protected void onPostExecute(Scenario scenario) {
            if (scenario != null) {
                callback.onScenarioLoaded(scenario);
            } else {
                callback.onDataNotAvailable();
            }
        }
    }

}
