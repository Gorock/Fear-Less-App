package com.fearless.prototype.fearless_change_cards.data.source;


import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

public interface TagDataSource {


    interface GetTagsCallback {

        void onTagsLoaded(List<Tag> tags);

        void onDataNotAvailable();

    }

    interface GetTagCallback {

        void onTagLoaded(Tag tag);

        void onDataNotAvailable();
    }


    void getTags(Long fearlessPatternId, @NonNull TagDataSource.GetTagsCallback callback);

    void getTag(@NonNull String tagName, @NonNull TagDataSource.GetTagCallback callback);

    void saveTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback);

    void removeTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback);
}
