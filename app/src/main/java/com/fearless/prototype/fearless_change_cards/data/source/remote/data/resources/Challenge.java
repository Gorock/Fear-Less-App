package com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;


@Resource
public class Challenge {
    private String title;
    private String description;
    private int thumbsUp;
    @JsonProperty("isResolved")
    private boolean isResolved;

    @Link
    private HALLink self;
    @Link
    private HALLink challenge;
    @Link
    private HALLink strategies;
    @Link
    private HALLink comments;
    @Link
    private HALLink creator;

    public Challenge() {
    }

    public Challenge(String title, String description, int thumbsUp, boolean isResolved, HALLink self) {
        this.title = title;
        this.description = description;
        this.thumbsUp = thumbsUp;
        this.isResolved = isResolved;
        this.self = self;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(int thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }

    public HALLink getChallenge() {
        return challenge;
    }

    public void setChallenge(HALLink challenge) {
        this.challenge = challenge;
    }

    public HALLink getStrategies() {
        return strategies;
    }

    public void setStrategies(HALLink strategies) {
        this.strategies = strategies;
    }

    public HALLink getComments() {
        return comments;
    }

    public void setComments(HALLink comments) {
        this.comments = comments;
    }

    public HALLink getCreator() {
        return creator;
    }

    public void setCreator(HALLink creator) {
        this.creator = creator;
    }
}
