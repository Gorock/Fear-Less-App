package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.fearless.prototype.fearless_change_cards.data.model.FearLessPatternTagJoin;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

@Dao
public interface FearlessTagsJoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FearLessPatternTagJoin fearLessPatternTagJoin);

    @Query("SELECT * FROM FearlessPattern INNER JOIN fearless_tag_join ON" +
            "(FearlessPattern.id = fearless_tag_join.patternId) WHERE " +
            "fearless_tag_join.tagName = :name")
    List<FearlessPattern> getPatternsWithTag(final String name);


    @Query("SELECT * FROM Tag INNER JOIN fearless_tag_join ON" +
            "(Tag.name = fearless_tag_join.tagName) WHERE " +
            "fearless_tag_join.patternId=:patternId")
    List<Tag> getTagsofPattern(final long patternId);


    @Delete
    void removeTagsOfPattern(FearLessPatternTagJoin join);
}
