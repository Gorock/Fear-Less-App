package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Tag;

public class TagRepository implements TagDataSource {


    private static TagRepository instance = null;

    private final TagDataSource localTagDataSource;


    private TagRepository(@NonNull TagDataSource tagDataSource) {
        this.localTagDataSource = tagDataSource;
    }

    public static TagRepository getINSTANCE(TagDataSource dataSource) {

        if (instance == null) {
            instance = new TagRepository(dataSource);
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }


    @Override
    public void getTags(Long fearlessPatternId, @NonNull GetTagsCallback callback) {
        this.localTagDataSource.getTags(fearlessPatternId, callback);
    }

    @Override
    public void getTag(@NonNull String tagName, @NonNull GetTagCallback callback) {
        this.localTagDataSource.getTag(tagName, callback);
    }


    @Override
    public void saveTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback) {
        this.localTagDataSource.saveTagWithPattern(tag, fearlessPatternId, callback);
    }

    @Override
    public void removeTagWithPattern(@NonNull Tag tag, @NonNull Long fearlessPatternId, GetTagsCallback callback) {
        this.localTagDataSource.removeTagWithPattern(tag, fearlessPatternId, callback);
    }
}
