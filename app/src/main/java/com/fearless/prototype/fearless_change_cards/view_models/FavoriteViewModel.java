package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Favorite;
import com.fearless.prototype.fearless_change_cards.data.source.local.FavoriteLocalDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

public class FavoriteViewModel extends AndroidViewModel {

    private FavoriteLocalDataSource favoriteLocalDataSource;

    public FavoriteViewModel(@NonNull Application application) {
        super(application);
        this.favoriteLocalDataSource = Injection.provideFavoriteLocalDataSource(application.getApplicationContext());
    }

    public MutableLiveData<Boolean> queryFavorite(final String resource) {
        final MutableLiveData<Boolean> result = new MutableLiveData<>();
        favoriteLocalDataSource.queryFavorite(resource, new FavoriteLocalDataSource.FavoriteCallback() {
            @Override
            public void onFavoriteQuerySuccess(boolean favorite) {
                result.setValue(favorite);
            }
        });
        return result;
    }

    public void addFavorite(String resouce) {
        favoriteLocalDataSource.addFavorite(new Favorite(resouce));
    }

    public void removeFavorite(String resouce) {
        favoriteLocalDataSource.removeFavorite(new Favorite(resouce));
    }
}
