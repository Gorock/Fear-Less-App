package com.fearless.prototype.fearless_change_cards.activities.view_helpers;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.view.View;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.MainActivity;
import com.fearless.prototype.fearless_change_cards.activities.library.GridViewActivity;
import com.fearless.prototype.fearless_change_cards.activities.scenario.ScenariosActivity;

public class BottomBar {


    private BottomBar() {
    }

    public static void addListenersToBar(BottomNavigationView view) {


        View backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                switchFromTo(context, MainActivity.class);
            }
        });


        View btnScenarios = view.findViewById(R.id.scenario_button);
        btnScenarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                switchFromTo(context, ScenariosActivity.class);
            }
        });

        View btnPattern = view.findViewById(R.id.pattern_button);
        btnPattern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                switchFromTo(context, GridViewActivity.class);
            }
        });


    }


    private static void switchFromTo(Context from, Class to) {
        Intent myIntent = new Intent(from, to);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        from.startActivity(myIntent);
    }


}
