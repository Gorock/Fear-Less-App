package com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemotePattern;

import java.util.List;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.EmbeddedResource;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
@JsonIgnoreProperties(ignoreUnknown = true)
public class PatternList extends ListResource {

    @EmbeddedResource("patterns")
    private List<RemotePattern> patterns;


    @Link
    private HALLink self;

    @Link
    HALLink profile;


    public List<RemotePattern> getPatterns() {
        return patterns;
    }

    public void setPatterns(List<RemotePattern> patterns) {
        this.patterns = patterns;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }
}
