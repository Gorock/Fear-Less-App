package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

/**
 * Main entry point for accessing Patterns data.
 * <p>
 * For simplicity, only getPatterns() and getObject() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new Pattern is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */

public interface PatternDataSource {

    interface GetPatternsCallback {

        void onPatternsLoaded(List<FearlessPattern> patterns);

        void onDataNotAvailable();

    }

    interface GetPatternCallback {

        void onPatternLoaded(FearlessPattern pattern);

        void onDataNotAvailable();
    }


    void searchPattern(@NonNull String searchTerm,
                       @NonNull GetPatternsCallback callback);


//    void getTags(long patternId, @NonNull TagDataSource.GetTagsCallback callback);

    void getPatterns(@NonNull GetPatternsCallback callback);

    void getPattern(long patternId, @NonNull GetPatternCallback callback);


    void savePattern(@NonNull FearlessPattern pattern);


    void refreshPatterns();

    void deletePattern(@NonNull String patternId);


}
