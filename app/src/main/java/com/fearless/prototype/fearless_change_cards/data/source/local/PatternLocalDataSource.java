package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.model.Tag;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource.GetTagsCallback;

import java.util.List;

public class PatternLocalDataSource implements PatternDataSource {


    private static volatile PatternLocalDataSource instance;
    private AsyncTask lastTask;
    private static volatile FearLessDao fearLessDao;

    private PatternLocalDataSource() {
    }

    public static PatternLocalDataSource getInstance(@NonNull FearLessDao fearLessDao) {
        if (instance == null) {
            synchronized (PatternLocalDataSource.class) {
                if (instance == null) {
                    instance = new PatternLocalDataSource();
                    PatternLocalDataSource.fearLessDao = fearLessDao;
                }
            }
        }
        return instance;
    }


    @Override
    public void searchPattern(@NonNull String searchTerm, @NonNull GetPatternsCallback callback) {
        searchTerm = "%" + searchTerm + "%";
        SearchPatternTask task = new SearchPatternTask(searchTerm, callback);
        if (lastTask != null)
            lastTask.cancel(true);
        lastTask = task;
        task.execute();
    }


    private static class SearchPatternTask extends AsyncTask<Void, Void, List<FearlessPattern>> {


        private final String searchTerm;
        private final GetPatternsCallback callback;

        SearchPatternTask(String searchTerm, GetPatternsCallback callback) {
            this.searchTerm = searchTerm;
            this.callback = callback;
        }


        @Override
        protected List<FearlessPattern> doInBackground(Void... voids) {
            return fearLessDao.searchPattern(searchTerm);
        }

        @Override
        protected void onPostExecute(List<FearlessPattern> patterns) {
            callback.onPatternsLoaded(patterns);

        }
    }

    @Override
    public void getPatterns(@NonNull final GetPatternsCallback callback) {
        GetPatterns getPatterns = new GetPatterns(callback);
        getPatterns.execute();
    }

    static class GetPatterns extends AsyncTask<Void, Void, List<FearlessPattern>> {

        GetPatternsCallback callback;

        GetPatterns(GetPatternsCallback callback) {
            super();
            this.callback = callback;
        }

        protected void setCallback(GetPatternsCallback callback) {
            this.callback = callback;
        }

        @Override
        protected List<FearlessPattern> doInBackground(Void... voids) {
            return fearLessDao.getPatterns();
        }

        @Override
        protected void onPostExecute(List<FearlessPattern> patterns) {
            this.callback.onPatternsLoaded(patterns);
        }
    }


    @Override
    public void getPattern(@NonNull long patternId, @NonNull final GetPatternCallback callback) {
        GetSinglePattern getSinglePattern = new GetSinglePattern(callback);
        getSinglePattern.execute(patternId);

    }

    static class GetSinglePattern extends AsyncTask<Long, Void, FearlessPattern> {

        GetPatternCallback callback;

        GetSinglePattern(GetPatternCallback callback) {
            super();
            this.callback = callback;

        }


        @Override
        protected FearlessPattern doInBackground(Long... longs) {
            return fearLessDao.getPattern(longs[0]);
        }

        @Override
        protected void onPostExecute(FearlessPattern pattern) {
            this.callback.onPatternLoaded(pattern);
        }
    }


    @Override
    public void savePattern(@NonNull FearlessPattern pattern) {
        AsyncTask<FearlessPattern, Void, Void> savePatternTask = new UpdatePatternsTask();
        savePatternTask.execute(pattern);
    }

    static class UpdatePatternsTask extends AsyncTask<FearlessPattern, Void, Void> {

        @Override
        protected Void doInBackground(FearlessPattern... fearlessPatterns) {
            fearLessDao.updatePattern(fearlessPatterns[0]);
            return null;

        }
    }

    @Override
    public void refreshPatterns() {
        throw new UnsupportedOperationException();
    }


    @Override
    public void deletePattern(@NonNull String patternId) {
        throw new UnsupportedOperationException();
    }


}