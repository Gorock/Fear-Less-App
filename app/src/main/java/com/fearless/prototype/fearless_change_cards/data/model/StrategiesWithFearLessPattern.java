package com.fearless.prototype.fearless_change_cards.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

public class StrategiesWithFearLessPattern {

    @Embedded
    public Strategy strategy;

    @Relation(parentColumn = "strategyId", entityColumn = "fstrategyId")
    public List<FearlessPattern> patternList;


}
