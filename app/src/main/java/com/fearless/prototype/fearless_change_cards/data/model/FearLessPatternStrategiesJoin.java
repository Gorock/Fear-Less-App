package com.fearless.prototype.fearless_change_cards.data.model;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

@Entity(tableName = "fearless_scenario_join",
        primaryKeys = {"strategyId", "patternId"},
        foreignKeys = {
                @ForeignKey(entity = FearlessPattern.class,
                        parentColumns = "id",
                        childColumns = "patternId"),
                @ForeignKey(entity = Strategy.class,
                        parentColumns = "id",
                        childColumns = "strategyId")
        })
public class FearLessPatternStrategiesJoin {

    @NonNull
    public final Long strategyId;
    @NonNull
    public final Long patternId;

    public FearLessPatternStrategiesJoin(long strategyId, Long patternId) {
        this.strategyId = strategyId;
        this.patternId = patternId;
    }
}

