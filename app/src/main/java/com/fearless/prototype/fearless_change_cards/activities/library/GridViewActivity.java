package com.fearless.prototype.fearless_change_cards.activities.library;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import com.fearless.prototype.fearless_change_cards.R;
import com.fearless.prototype.fearless_change_cards.activities.view_helpers.PatternCardViewFactory;
import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.view_models.FavoriteViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.PatternViewModel;
import com.fearless.prototype.fearless_change_cards.view_models.SearchPatternsViewModel;

import java.util.List;

public class GridViewActivity extends AppCompatActivity implements PatternShower {

    private GridLayout gridLayout;

    private PatternCardViewFactory patternCardViewFactory;
    private FavoriteViewModel favoriteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libary_grid);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);


        PatternViewModel patternViewModel = ViewModelProviders.of(this).get(PatternViewModel.class);
        favoriteViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);

        patternViewModel.getPatterns().observe(this, new Observer<List<FearlessPattern>>() {
            @Override
            public void onChanged(@Nullable List<FearlessPattern> patterns) {
                showPatterns(patterns);
            }
        });


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.pattern_button);


        BottomNavigationView topView = findViewById(R.id.top_view_selection);
        LibaryTopNavigation.createTopNavigation(topView, this, R.id.GridButton);

        this.gridLayout = findViewById(R.id.GridLayout);
        patternCardViewFactory = new PatternCardViewFactory(this, this,
                R.layout.template_pattern_card,
                gridLayout, patternViewModel, favoriteViewModel);

        PatternSearcher.makePatternSearcher(this,
                (SearchView) findViewById(R.id.searchView),
                ViewModelProviders.of(this).get(SearchPatternsViewModel.class),
                this);
    }


    public void showPatterns(List<FearlessPattern> patterns) {
        if (patterns == null)
            return;

        this.gridLayout.removeAllViews();

        for (FearlessPattern pattern : patterns) {
            this.gridLayout.addView(patternCardViewFactory.makeCard(pattern));

        }
    }
}
