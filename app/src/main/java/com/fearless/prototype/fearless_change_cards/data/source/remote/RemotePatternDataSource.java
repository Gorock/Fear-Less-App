package com.fearless.prototype.fearless_change_cards.data.source.remote;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources.PatternList;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemotePattern;
import com.fearless.prototype.fearless_change_cards.data.source.remote.mappers.PatternMapper;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;

public class RemotePatternDataSource implements PatternDataSource {


    private PatternRestClient patternRestClient;

    public RemotePatternDataSource() {
        Retrofit retrofit = Injection.provideRetrofit();
        patternRestClient = retrofit.create(PatternRestClient.class);
    }


    @Override
    public void searchPattern(@NonNull final String searchTerm, @NonNull final GetPatternsCallback callback) {
        getPatterns(new GetPatternsCallback() {
            @Override
            public void onPatternsLoaded(List<FearlessPattern> patterns) {
                List<FearlessPattern> searchResults = new ArrayList<>();

                for (FearlessPattern pattern : patterns) {
                    if (pattern.matchSearchTerm(searchTerm)) {
                        searchResults.add(pattern);
                    }
                }
                callback.onPatternsLoaded(searchResults);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
    }


    @Override
    public void getPatterns(@NonNull GetPatternsCallback callback) {
        GetAllPatterns task = new GetAllPatterns(callback, patternRestClient);
        task.execute();
    }

    public static class GetAllPatterns extends AsyncTask<Void, Void, List<FearlessPattern>> {

        GetPatternsCallback callback;
        PatternRestClient restClient;

        GetAllPatterns(GetPatternsCallback callback, PatternRestClient restClient) {
            super();
            this.callback = callback;
            this.restClient = restClient;
        }

        @Override
        protected void onPostExecute(List<FearlessPattern> patterns) {
            if (patterns.isEmpty())
                this.callback.onDataNotAvailable();
            else
                this.callback.onPatternsLoaded(patterns);
        }

        @Override
        protected List<FearlessPattern> doInBackground(Void... voids) {
            try {

                Response<PatternList> patternsRespone = restClient.getAllPatterns().execute();
                if (patternsRespone.isSuccessful()) {
                    PatternList patterns = patternsRespone.body();
                    int id = 0;
                    List<FearlessPattern> patternList = PatternMapper.map(patterns.getPatterns());
                    for (FearlessPattern pattern : patternList) {
                        pattern.setId(id);
                        id++;
                    }
                    return patternList;
                }
                return new ArrayList<>();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new ArrayList<>();
        }


    }


    @Override
    public void getPattern(long patternId, @NonNull GetPatternCallback callback) {
        GetPatternAtIndex task = new GetPatternAtIndex(patternId, callback, this.patternRestClient);
        task.execute();
    }


    static class GetPatternAtIndex extends AsyncTask<Void, Void, FearlessPattern> {

        GetPatternCallback callback;
        PatternRestClient restClient;
        Long patternIndex;

        GetPatternAtIndex(Long patternId, GetPatternCallback callback, PatternRestClient restClient) {
            super();
            this.callback = callback;
            this.restClient = restClient;
            this.patternIndex = patternId;
        }

        @Override
        protected void onPostExecute(FearlessPattern pattern) {
            if (pattern != null)
                callback.onPatternLoaded(pattern);
            else
                callback.onDataNotAvailable();
        }

        @Override
        protected FearlessPattern doInBackground(Void... voids) {
            Response<PatternList> patternsRespone = null;
            try {
                if (patternIndex < 0) // spring data rest gives the first element when value is negative
                    return null;
                patternsRespone = restClient.getPatternAtIndex(patternIndex).execute();
                if (patternsRespone.isSuccessful()) {
                    List<RemotePattern> patterns = patternsRespone.body().getPatterns();
                    if (patterns.isEmpty()) {
                        return null;
                    }
                    return PatternMapper.map(patterns.get(0), patternIndex);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }


    @Override
    public void savePattern(@NonNull FearlessPattern pattern) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void refreshPatterns() {
        throw new UnsupportedOperationException();
    }


    @Override
    public void deletePattern(@NonNull String patternId) {
        throw new UnsupportedOperationException();
    }


}
