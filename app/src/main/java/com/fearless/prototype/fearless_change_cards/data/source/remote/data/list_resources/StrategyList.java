package com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.RemoteStrategy;

import java.util.List;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.EmbeddedResource;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
@JsonIgnoreProperties(ignoreUnknown = true)
public class StrategyList extends ListResource {


    @EmbeddedResource
    private List<RemoteStrategy> strategies;


    @Link
    HALLink self;

    @Link
    HALLink profile;

    public List<RemoteStrategy> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<RemoteStrategy> strategies) {
        this.strategies = strategies;
    }
}

