package com.fearless.prototype.fearless_change_cards.data.source.remote.data.list_resources;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources.Challenge;

import java.util.List;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.EmbeddedResource;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChallangeList extends ListResource {

    @EmbeddedResource("challenges")
    private List<Challenge> challenges;


    @Link
    private HALLink self;

    @Link
    private HALLink profile;


    public ChallangeList() {

    }

    public ChallangeList(List<Challenge> challenges, HALLink self, HALLink profile) {
        this.challenges = challenges;
        this.self = self;
        this.profile = profile;
    }


    public List<Challenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }

    public HALLink getProfile() {
        return profile;
    }

    public void setProfile(HALLink profile) {
        this.profile = profile;
    }
}
