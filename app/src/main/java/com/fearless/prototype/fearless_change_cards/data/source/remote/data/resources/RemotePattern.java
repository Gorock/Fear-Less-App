package com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
public class RemotePattern {

    @JsonProperty("imagePath")
    private Object imagePath;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("problem")
    private String problem;
    @JsonProperty("solution")
    private String solution;

    @Link
    private HALLink self;
    @Link
    private HALLink pattern;
    @Link
    private HALLink strategies;


    public RemotePattern() {

    }


    public Object getImagePath() {
        return imagePath;
    }

    public void setImagePath(Object imagePath) {
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }

    public HALLink getPattern() {
        return pattern;
    }

    public void setPattern(HALLink pattern) {
        this.pattern = pattern;
    }

    public HALLink getStrategies() {
        return strategies;
    }

    public void setStrategies(HALLink strategies) {
        this.strategies = strategies;
    }
}
