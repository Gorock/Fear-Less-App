package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.fearless.prototype.fearless_change_cards.data.model.Tag;

import java.util.List;

@Dao
public interface TagsDao {


    @Query("Select * from Tag")
    List<Tag> getTags();


    @Query("Select * from Tag where name=:name")
    Tag findTagByName(String name);


    @Insert
    void insertTagList(List<Tag> tags);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Tag tag);

    @Update
    void updateTag(Tag tag);
}
