package com.fearless.prototype.fearless_change_cards.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;

import java.util.List;

@Dao
public interface PatternNoteDao {

    @Query("SELECT * from patternnote where patternId = :patternId")
    List<PatternNote> findPatternNoteForPatternWithId(long patternId);

    @Insert
    void addPersonalNote(PatternNote note);


    @Delete
    void removePatternNote(PatternNote note);

}
