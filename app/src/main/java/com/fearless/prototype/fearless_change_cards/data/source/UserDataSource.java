package com.fearless.prototype.fearless_change_cards.data.source;

import com.fearless.prototype.fearless_change_cards.data.model.User;

public interface UserDataSource {
    interface GetUserCallback {

        void onUserLoaded(User user);

        void onDataNotAvailable();
    }
}
