package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.model.User;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class ScenarioViewModel extends AndroidViewModel {

    private MutableLiveData<List<Scenario>> liveScenarioListData;
    private ScenarioDataSource scenarioDataSource;


    public ScenarioViewModel(@NonNull Application application) {
        super(application);
        this.scenarioDataSource = Injection.provideScenarioRepository(application.getApplicationContext());
    }


    public LiveData<List<Scenario>> getScenarios() {
        if (liveScenarioListData == null)
            liveScenarioListData = new MutableLiveData<>();

        scenarioDataSource.getScenarios(
                new ScenarioDataSource.GetScenariosCallback() {
                    @Override
                    public void onScenariosLoaded(List<Scenario> scenarios) {
                        liveScenarioListData.setValue(scenarios);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        throw new UnsupportedOperationException();

                    }
                });
        return liveScenarioListData;
    }


    public LiveData<Scenario> changeScenarioFavoriteStatus(Scenario scenario, boolean favorite) {
        final MutableLiveData<Scenario> changedScenario = new MutableLiveData<>();
        scenario.setIsFavorite(favorite);
        scenarioDataSource.saveScenario(scenario, new ScenarioDataSource.GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                changedScenario.setValue(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException("not implemented yet");
            }
        });

        return changedScenario;
    }


    public LiveData<Scenario> addScenario(String name, String description, String problem, String creatorLink) {
        final MutableLiveData<Scenario> scenarioMutableLiveData = new MutableLiveData<>();
        Scenario scenario = new Scenario(name, description, problem);
        scenario.setCreatorLink(creatorLink);
        scenarioDataSource.insertScenario(
                scenario,
                new ScenarioDataSource.GetScenarioCallback() {
                    @Override
                    public void onScenarioLoaded(Scenario scenario) {
                        scenarioMutableLiveData.setValue(scenario);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        throw new UnsupportedOperationException();

                    }
                });
        return scenarioMutableLiveData;
    }


    public LiveData<Scenario> addValueToThumbsUp(Scenario scenario, int value) {
        // will not work with rest (dont know if already gave thumb up
        scenario.setThumbsUp(scenario.getThumbsUp() + value);
        final MutableLiveData<Scenario> scenarioMutableLiveData = new MutableLiveData<>();
        scenarioDataSource.saveScenario(scenario, new ScenarioDataSource.GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                scenarioMutableLiveData.setValue(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return scenarioMutableLiveData;
    }

    public LiveData<Scenario> getScenarioWithUriThenId(String uri, final Long id) {
        final MutableLiveData<Scenario> scenarioLiveData = new MutableLiveData<>();
        if (uri == null) {
            getScenarioWithId(id);
            return scenarioLiveData;
        }
        scenarioDataSource.getScenarioWithUri(uri, new ScenarioDataSource.GetScenarioCallback() {
            @Override
            public void onScenarioLoaded(Scenario scenario) {
                scenarioLiveData.setValue(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                getScenarioWithId(id);
            }
        });
        return scenarioLiveData;
    }


    public MutableLiveData<Scenario> getScenarioWithId(Long id) {
        final MutableLiveData<Scenario> scenarioLiveData = new MutableLiveData<>();
        scenarioDataSource.getScenario(String.valueOf(id), new ScenarioDataSource.GetScenarioCallback() {

            @Override
            public void onScenarioLoaded(Scenario scenario) {
                scenarioLiveData.setValue(scenario);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return scenarioLiveData;
    }

    public MutableLiveData<Boolean> deleteScenario(Scenario scenario) {
        final MutableLiveData<Boolean> successful = new MutableLiveData<>();
        scenarioDataSource.deleteScenario(scenario, new ScenarioDataSource.DeleteScenarioCallback() {
            @Override
            public void onSucess() {
                successful.setValue(true);
            }

            @Override
            public void onFailure() {
                successful.setValue(false);
            }
        });
        return successful;
    }

    public MutableLiveData<User> getCreatorOfScenario(Scenario scenario) {
        final MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();
        scenarioDataSource.getScenarioCreator(scenario, new ScenarioDataSource.ScenarioCreatorCallback() {
            @Override
            public void onCreatorFound(User user) {
                userMutableLiveData.setValue(user);
            }
        });
        return userMutableLiveData;
    }
}
