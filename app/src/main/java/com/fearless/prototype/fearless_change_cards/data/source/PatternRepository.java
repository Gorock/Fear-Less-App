package com.fearless.prototype.fearless_change_cards.data.source;

import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.TagDataSource.GetTagsCallback;
import com.fearless.prototype.fearless_change_cards.data.source.local.PatternDatabase;
import com.fearless.prototype.fearless_change_cards.data.source.local.PatternLocalDataSource;

import java.util.List;

public class PatternRepository implements PatternDataSource {

    private static PatternRepository instance = null;

    private final PatternDataSource patternSqlLiteSource;
    private final PatternDataSource remotePatternSource;


    private PatternRepository(@NonNull PatternDataSource patternSqlLiteSource,
                              @NonNull PatternDataSource remotePatternSource) {
        this.patternSqlLiteSource = patternSqlLiteSource;
        this.remotePatternSource = remotePatternSource;

    }

    public static PatternRepository getINSTANCE(PatternDataSource instance) {
        return PatternRepository.getINSTANCE(instance, instance);
    }

    public static PatternRepository getINSTANCE(PatternDataSource patternSqlLiteSource,
                                                PatternDataSource remotePatternSource) {

        if (instance == null) {
            instance = new PatternRepository(patternSqlLiteSource, remotePatternSource);
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    @Override
    public void searchPattern(@NonNull String searchTeam, @NonNull GetPatternsCallback callback) {
        this.patternSqlLiteSource.searchPattern(searchTeam, callback);
    }


    @Override
    public void getPatterns(@NonNull final GetPatternsCallback callback) {

        remotePatternSource.getPatterns(new GetPatternsCallback() {
            @Override
            public void onPatternsLoaded(List<FearlessPattern> patterns) {
                callback.onPatternsLoaded(patterns);
            }

            @Override
            public void onDataNotAvailable() {
                patternSqlLiteSource.getPatterns(callback);
            }
        });
    }


    @Override
    public void getPattern(final long patternId, @NonNull final GetPatternCallback callback) {
        remotePatternSource.getPattern(patternId, new GetPatternCallback() {
            @Override
            public void onPatternLoaded(FearlessPattern pattern) {
                callback.onPatternLoaded(pattern);
            }

            @Override
            public void onDataNotAvailable() {
                patternSqlLiteSource.getPattern(patternId, callback);

            }
        });
    }


    @Override
    public void savePattern(@NonNull FearlessPattern pattern) {
        patternSqlLiteSource.savePattern(pattern);
    }


    @Override
    public void refreshPatterns() {
        throw new UnsupportedOperationException();
    }


    @Override
    public void deletePattern(@NonNull String patternId) {
        throw new UnsupportedOperationException();
    }

}
