package com.fearless.prototype.fearless_change_cards.data.source.remote.data.resources;


import com.fasterxml.jackson.annotation.JsonProperty;

import io.openapitools.jackson.dataformat.hal.HALLink;
import io.openapitools.jackson.dataformat.hal.annotation.Link;
import io.openapitools.jackson.dataformat.hal.annotation.Resource;

@Resource
public class RemoteStrategy {
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("thumbsUp")
    private Long thumbsUp;


    @Link
    private HALLink self;
    @Link
    private HALLink patterns;
    @Link
    private HALLink strategy;

    @Link
    private HALLink comments;

    @Link
    private HALLink challenge;

    @Link
    private HALLink creator;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(Long thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    public HALLink getSelf() {
        return self;
    }

    public void setSelf(HALLink self) {
        this.self = self;
    }


    public HALLink getStrategy() {
        return strategy;
    }

    public void setStrategy(HALLink strategy) {
        this.strategy = strategy;
    }

    public HALLink getCreator() {
        return creator;
    }

    public void setCreator(HALLink creator) {
        this.creator = creator;
    }

    public HALLink getComments() {
        return comments;
    }

    public void setComments(HALLink comments) {
        this.comments = comments;
    }

    public HALLink getPatterns() {
        return patterns;
    }

    public void setPatterns(HALLink patterns) {
        this.patterns = patterns;
    }

    public HALLink getChallenge() {
        return challenge;
    }

    public void setChallenge(HALLink challenge) {
        this.challenge = challenge;
    }
}
