package com.fearless.prototype.fearless_change_cards.view_models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.fearless.prototype.fearless_change_cards.data.model.PatternNote;
import com.fearless.prototype.fearless_change_cards.data.source.PatternNoteDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import java.util.List;

public class PatternNoteViewModel extends AndroidViewModel {

    private PatternNoteDataSource noteDataSource;

    private MutableLiveData<List<PatternNote>> noteMutableLiveData;

    public PatternNoteViewModel(@NonNull Application application) {
        super(application);
        this.noteDataSource = Injection.providePatternNoteRepository(application.getApplicationContext());
    }


    public LiveData<List<PatternNote>> getNotesForPatternWithId(Long id) {
        if (noteMutableLiveData == null)
            noteMutableLiveData = new MutableLiveData<>();
        noteDataSource.getNoteWithPatternId(id, new PatternNoteDataSource.GetNotesCallback() {
            @Override
            public void onNotesLoaded(List<PatternNote> notes) {
                noteMutableLiveData.setValue(notes);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException();
            }
        });
        return noteMutableLiveData;
    }

    public LiveData<List<PatternNote>> saveNote(final PatternNote note) {
        if (noteMutableLiveData == null)
            noteMutableLiveData = new MutableLiveData<>();
        noteDataSource.saveNote(note, new PatternNoteDataSource.GetNotesCallback() {
            @Override
            public void onNotesLoaded(List<PatternNote> noteList) {
                noteMutableLiveData.setValue(noteList);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException("change it when it is called");
            }
        });
        return noteMutableLiveData;
    }

    public LiveData<List<PatternNote>> removeNote(PatternNote patternNote) {
        if (noteMutableLiveData == null)
            noteMutableLiveData = new MutableLiveData<>();
        noteDataSource.removeNote(patternNote, new PatternNoteDataSource.GetNotesCallback() {
            @Override
            public void onNotesLoaded(List<PatternNote> noteList) {
                noteMutableLiveData.setValue(noteList);
            }

            @Override
            public void onDataNotAvailable() {
                throw new UnsupportedOperationException("change it when it is called");
            }
        });
        return noteMutableLiveData;
    }
}
