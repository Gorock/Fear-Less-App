package com.fearless.prototype.fearless_change_cards;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.fearless.prototype.fearless_change_cards.activities.scenario.ScenariosActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasMinimumChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ScenariosTest {
    @Rule
    public ActivityTestRule<ScenariosActivity> mActivityRule = new ActivityTestRule<>(
            ScenariosActivity.class);

    @Before
    public void initValidString() {
    }


    @Test
    public void test_if_scenarios_are_shown() {
        onView(withId(R.id.scenario_list)).check(matches(hasMinimumChildCount(10)));
    }

    @Test
    public void test_if_can_back_to_Pattern() {
        onView(withId(R.id.pattern_button)).perform(click());
    }

    @Test
    public void test_if_can_click_for_detail_view() {
        onView(withChild(withText("3"))).perform(scrollTo()).perform(click());
        onView(withId(R.id.scenarioPlace)).check(matches(withId(R.id.scenarioPlace)));
    }



}
