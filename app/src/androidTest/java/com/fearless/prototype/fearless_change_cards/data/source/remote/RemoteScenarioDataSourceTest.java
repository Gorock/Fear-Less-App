package com.fearless.prototype.fearless_change_cards.data.source.remote;

import com.fearless.prototype.fearless_change_cards.data.model.Scenario;
import com.fearless.prototype.fearless_change_cards.data.source.ScenarioDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class RemoteScenarioDataSourceTest {

    private CountDownLatch signal;
    private ScenarioDataSource remoteDatasource;

    @Before
    public void setUp() throws Exception {
        Retrofit retrofit = Injection.provideRetrofit();
        PatternRestClient restClient = retrofit.create(PatternRestClient.class);
        signal = new CountDownLatch(1);
        remoteDatasource = new RemoteScenarioDataSource("DEBUG");
    }

    @After
    public void tearDown() throws Exception {
        signal.countDown();
    }

    @Test
    public void getScenarios() throws InterruptedException {
        remoteDatasource.getScenarios(new ScenarioDataSource.GetScenariosCallback() {
            @Override
            public void onScenariosLoaded(List<Scenario> scenarios) {
                assertThat(scenarios, is(not(nullValue())));
                assertThat(scenarios, is(not(empty())));
                signal.countDown();
            }

            @Override
            public void onDataNotAvailable() {
                fail("failed to retrive data");
                signal.countDown();
            }
        });
        signal.await(3, TimeUnit.SECONDS);
        assertThat(signal.getCount(), equalTo(0L));
    }

    @Test
    public void getScenario() throws InterruptedException {
        remoteDatasource.getScenario("0", new ScenarioDataSource.GetScenarioCallback() {

            @Override
            public void onScenarioLoaded(Scenario scenario) {
                assertThat(scenario, is(not(nullValue())));
                assertThat(scenario.getPrimaryId(), is(notNullValue()));
                signal.countDown();

            }

            @Override
            public void onDataNotAvailable() {
                fail("failed to retrive data");
                signal.countDown();

            }
        });

        signal.await(3, TimeUnit.SECONDS);
        assertThat(signal.getCount(), equalTo(0L));
    }
}
