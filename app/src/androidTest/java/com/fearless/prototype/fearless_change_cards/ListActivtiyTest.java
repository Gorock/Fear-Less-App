package com.fearless.prototype.fearless_change_cards;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.fearless.prototype.fearless_change_cards.activities.library.ListViewActivity;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class ListActivtiyTest {

    @Rule
    public ActivityTestRule<ListViewActivity> mActivityRule = new ActivityTestRule<>(
            ListViewActivity.class);

    @Before
    public void initValidString() {
    }


    @Test
    public void test_if_pattern_are_shown() {
        onView(withId(R.id.cardList)).check(ViewAssertions.matches(hasChildCount(58)));
    }

    @Test
    public void test_if_pattern_is_clickable() {
        onView(withChild(withText("20"))).perform(scrollTo()).perform(click());
    }

    @Test
    public void test_if_pattern_is_shown_after_click(){
        onView(withChild(withText("20"))).perform(scrollTo()).perform(click());
        onView(withId(R.id.pattern_title)).check(ViewAssertions.matches(ViewMatchers.withText("Corporate Angel")));
    }

    @Test
    public void test_switch_to_grid() {
        // Type text and then press the button.
        onView(withId(R.id.GridButton))
                .perform(click());
    }


    @Test
    public void test_switch_to_list() {
        // Type text and then press the button.
        onView(withId(R.id.ListButton))
                .perform(click());
    }


    @Test
    public void test_switch_to_scenarios() {
        // Type text and then press the button.
        onView(withId(R.id.scenario_button))
                .perform(click());

        onView(withId(R.id.addScenario));
    }


}
