package com.fearless.prototype.fearless_change_cards;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import com.fearless.prototype.fearless_change_cards.activities.library.GridViewActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.hasMinimumChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.withChild;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class GridActivityTest {

    @Rule
    public ActivityTestRule<GridViewActivity> mActivityRule = new ActivityTestRule<>(
            GridViewActivity.class);

    @Before
    public void initValidString() {
    }


    @Test
    public void test_switch_to_grid() {
        // Type text and then press the button.
        onView(withId(R.id.GridButton))
                .perform(click());

        onView(withId(R.id.GridLayout));
    }

    @Test
    public void test_switch_to_list() {
        // Type text and then press the button.
        onView(withId(R.id.ListButton))
                .perform(click());
    }


    @Test
    public void test_if_pattern_are_shown() {
        onView(withId(R.id.GridLayout)).check(ViewAssertions.matches(hasChildCount(58)));
    }

    @Test
    public void test_if_pattern_is_clickable() {
        onView(withChild(withText("20"))).perform(scrollTo()).perform(click());
    }

    @Test
    public void test_if_pattern_is_shown_after_click() {
        onView(withChild(withText("20"))).perform(scrollTo()).perform(click());
        onView(withId(R.id.pattern_title)).check(ViewAssertions.matches(ViewMatchers.withText("Corporate Angel")));
    }

    @Test
    public void test_if_pattern_has_tags() {
        onView(withChild(withText("1"))).perform(scrollTo()).perform(click());
        onView(withId(R.id.tagLayout)).check(ViewAssertions.matches(hasMinimumChildCount(2)));
    }

    @Test
    public void test_switch_to_scenarios() {
        // Type text and then press the button.
        onView(withId(R.id.scenario_button))
                .perform(click());

        onView(withId(R.id.addScenario));
    }

}
