package com.fearless.prototype.fearless_change_cards.data.source.remote;

import com.fearless.prototype.fearless_change_cards.data.model.FearlessPattern;
import com.fearless.prototype.fearless_change_cards.data.source.PatternDataSource;
import com.fearless.prototype.fearless_change_cards.injection.Injection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

import retrofit2.Retrofit;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


public class RemotePatternDataSourceTest {


    PatternDataSource remoteDatasource;

    PatternRestClient restClient;
    private CountDownLatch signal;

    private List<FearlessPattern> patternList;

    @Before
    public void setUp() throws Exception {
        Retrofit retrofit = Injection.provideRetrofit();
        restClient = retrofit.create(PatternRestClient.class);
        signal = new CountDownLatch(1);
        remoteDatasource = new RemotePatternDataSource();
    }


    @After
    public void tearDown() throws Exception {
        signal.countDown();
    }


    @Test
    public void getPatterns() throws InterruptedException {
        final List<FearlessPattern> patternList;
        new RemotePatternDataSource.GetAllPatterns(new PatternDataSource.GetPatternsCallback() {


            @Override
            public void onPatternsLoaded(List<FearlessPattern> patterns) {
                assertThat(patterns, is(not(empty())));
                patterns.forEach(new Consumer<FearlessPattern>() {
                    @Override
                    public void accept(FearlessPattern pattern) {
                        assertThat(pattern.getId(), notNullValue());
                    }
                });
                signal.countDown();
            }

            @Override
            public void onDataNotAvailable() {
                fail("Should not fail here ");
                signal.countDown();
            }
        }, restClient).execute();

        signal.await();

    }

    @Test
    public void getPattern() throws InterruptedException {

        new RemotePatternDataSource.GetPatternAtIndex(0L, new PatternDataSource.GetPatternCallback() {
            @Override
            public void onPatternLoaded(FearlessPattern pattern) {
                signal.countDown();
                assertThat(pattern, is(notNullValue()));
                assertThat(pattern.getId(), equalTo(0L));
            }

            @Override
            public void onDataNotAvailable() {
                assertThat("No patterns or connection failed ?", null, notNullValue());
            }
        }, restClient).execute();

        signal.await();
    }
}


