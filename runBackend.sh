#!/usr/bin/env bash

systemctl is-active --quiet docker
if [[ $? != 0 ]]; then
    systemctl start docker
fi

docker pull registry.gitlab.com/ss17-gp-fear-less/spring-backend:master \
    && docker run -p 8080:8080 registry.gitlab.com/ss17-gp-fear-less/spring-backend:master

